package edu.duke.ece651.mp.server;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

public class ServerTest {
  @Test
  public void test_factorial()throws IOException, InterruptedException  {
    Thread th = new Thread(() ->{
        try{
          String [] args = {"2"};
    Server.main(args);
        }catch(IOException | InterruptedException | ClassNotFoundException e){
          // System.out.println(e.getMessage());
        }
    });
    
    th.start();
    Thread.sleep(200);
    Socket s1 = new Socket("localhost", 12345);
    Socket s2 = new Socket("localhost", 12345);

    s1.close();
    s2.close();
  }

  @Test
  public void test_1()throws IOException, InterruptedException  {
    Thread th = new Thread(() ->{
        try{
          String [] args = {"2", "hello"};
    Server.main(args);
        }catch(IOException | InterruptedException | ClassNotFoundException e){
          //  System.out.println(e.getMessage());
        }
    });
    
    th.start();
    Thread.sleep(200);
    Socket s1 = new Socket("localhost", 12345);
    Socket s2 = new Socket("localhost", 12345);

    s1.close();
    s2.close();
  }
@Test
  public void test_2()throws IOException, InterruptedException  {
    Thread th = new Thread(() ->{
        try{
          String [] args = {"1"};
    Server.main(args);
        }catch(IOException | InterruptedException | ClassNotFoundException e){
          //  System.out.println(e.getMessage());
        }
    });
    
    th.start();
    Thread.sleep(200);
    Socket s1 = new Socket("localhost", 12345);
    Socket s2 = new Socket("localhost", 12345);

    s1.close();
    s2.close();
  }

  @Test
  public void test() {
    List<Integer> list = new ArrayList<>();
    list.add(1); list.add(2); list.add(3);
    Map<Integer, Integer> map = new HashMap<>();
    map.put(1, 3); map.put(2, 3); map.put(3, 6);
    System.out.println(list.stream().filter(id -> map.get(id) % 2 == 0).findFirst().get());
  }

  
}
