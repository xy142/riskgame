package edu.duke.ece651.mp.server;

import edu.duke.ece651.mp.common.Board;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * This class serves as a database for evol2
 */
public class UserDB {
    Map<String, String> db;
    Map<String, Boolean> loginStatus;
    public UserDB(){
        db = new ConcurrentHashMap<>();
        loginStatus = new ConcurrentHashMap<>();
    }

    public String signUp(String name, String password){
        if(db.containsKey(name)){
            return "Wrong:The username is used";
        }
        db.put(name, password);
        loginStatus.put(name, false);
        return null;
    }

    /**
     * check user name and password for user login
     * @param name
     * @param password
     * @return
     */
    public String logIn(String name, String password ){
        if(!db.containsKey(name)){
            return "Username does not exist!";
        }
        if (!db.get(name).equals(password)){
            return "Password authentication failed!";
        }
        if (loginStatus.get(name)) {
            return "This account has already logged in.";
        }
        loginStatus.put(name, true);
        return null;
    }

    public void logOut(String name) {
        loginStatus.put(name, false);
    }



}
