package edu.duke.ece651.mp.server;

import edu.duke.ece651.mp.common.Board;
import edu.duke.ece651.mp.common.ConnectionHandler;
import edu.duke.ece651.mp.common.RoomInfo;
import edu.duke.ece651.mp.common.packet.*;

import javax.sound.midi.Soundbank;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class ServerPacketHandler implements PacketHandler {
    private HashMap<ConnectionHandler, String> connectionHandlerToUsername;
    private ConcurrentHashMap<Integer, ConnectionHandler> uuidToConnectionHandler;
    private ConcurrentHashMap<String, ConnectionHandler> usernameToConnectionHandler;
    private AtomicInteger playerIdCounter;
    private AtomicInteger gameIdCounter;
    private HashMap<Integer, ServerGame<?>> gameIdToGame;
    private HashMap<String, List<Integer>> usernameToPlayerIdList;
    private HashMap<Integer, String> playerIdToUsername;
    private HashMap<Integer, Integer> playerIdToGameId;
    private HashMap<String, Integer> usernameToFocusGameId;
    private UserDB userDB;
    private ArrayBlockingQueue<Packet> out;
    public <T> ServerPacketHandler(ConcurrentHashMap<Integer, ConnectionHandler> uuidToConnectionHandler, ConcurrentHashMap<String, ConnectionHandler> usernameToConnectionHandler, ArrayBlockingQueue<Packet> out) {
        connectionHandlerToUsername = new HashMap<>();
        this.uuidToConnectionHandler = uuidToConnectionHandler;
        this.usernameToConnectionHandler = usernameToConnectionHandler;
        gameIdToGame = new HashMap<>();
        usernameToPlayerIdList = new HashMap<>();
        playerIdToUsername = new HashMap<>();
        playerIdToGameId = new HashMap<>();
        usernameToFocusGameId = new HashMap<>();
        userDB = new UserDB();
        playerIdCounter = new AtomicInteger(0);
        gameIdCounter = new AtomicInteger(0);
        this.out = out;
    }
    @Override
    public <T> void visit(BoardPacket<T> p) {
        ServerGame<T> game = (ServerGame<T>) gameIdToGame.get(playerIdToGameId.get(p.getPlayerId()));
        game.storeBoard(p.getPlayerId(), p.getBoard());
        //throw new IllegalArgumentException("Server will never receive a board packet.");
    }

    @Override
    public void visit(PlayerPacket p) {
        throw new IllegalArgumentException("Server will never receive a player packet.");
    }

    @Override
    public synchronized void visit(ActionPacket p) throws InterruptedException {
        ServerGame<?> game = (ServerGame<?>) gameIdToGame.get(playerIdToGameId.get(p.getPlayerId()));
        game.receiveAction(p.getAction());
    }

    @Override
    public void visit(CheckResultPacket p) {
        throw new IllegalArgumentException("Server will never receive a check result packet.");

    }

    @Override
    public void visit(PlacePacket p) throws IOException, InterruptedException {
        throw new IllegalArgumentException("Server will never receive a place packet.");
    }

    @Override
    public void visit(MainPacket p) throws IOException {
        throw new IllegalArgumentException("Server will never receive a main packet.");
    }

    @Override
    public void visit(WaitPacket waitPacket) {
        throw new IllegalArgumentException("Server will never receive a wait packet.");
    }

    @Override
    public void visit(EndPacket p) {
        throw new IllegalArgumentException("Server will never receive an end packet.");
    }

    @Override
    public void visit(MessagePacket p) {
        throw new IllegalArgumentException("Server will never receive a message packet.");
    }
    
    @Override
    public  synchronized void visit(CreateRoomPacket createRoomPacket) throws InterruptedException {
        //TODO
        int gameId = gameIdCounter.getAndIncrement();
        int playerId = playerIdCounter.getAndIncrement();
        String username = createRoomPacket.getUsername();
        ServerGame<?> game = new ServerGame<>(gameId, username, playerId, createRoomPacket.getRoomSize(), out);
        gameIdToGame.put(gameId, game);
        usernameToPlayerIdList.get(username).add(playerId);
        playerIdToUsername.put(playerId, username);
        playerIdToGameId.put(playerId, gameId);
        usernameToFocusGameId.put(username, gameId);
        out.put(new RoomListPacket(username, -1, getRoomList(username)));
    }

    @Override
    public synchronized void visit(SignUpPacket signUpPacket) throws InterruptedException {
        //TODO
        String errorMsg = userDB.signUp(signUpPacket.getUsername(), signUpPacket.getPassword());
        if (errorMsg == null)
            usernameToPlayerIdList.put(signUpPacket.getUsername(), new ArrayList<>());
        Packet packet = new SignUpResultPacket("", -1, errorMsg);
        packet.setUuid(signUpPacket.getUuid());
        out.put(packet);
    }

    @Override
    public synchronized void visit(LogInPacket logInPacket) throws InterruptedException {
        //TODO
        String errorMsg = userDB.logIn(logInPacket.getUsername(), logInPacket.getPassword());
        Packet packet;
        if (errorMsg != null) {
            packet = new LogInFailurePacket("", -1, errorMsg);
        } else {
            usernameToConnectionHandler.put(logInPacket.getUsername(), uuidToConnectionHandler.get(logInPacket.getUuid()));
            connectionHandlerToUsername.put(uuidToConnectionHandler.get(logInPacket.getUuid()), logInPacket.getUsername());
            usernameToFocusGameId.put(logInPacket.getUsername(), -1);
            packet = new RoomListPacket(logInPacket.getUsername(), -1, getRoomList(logInPacket.getUsername()));
        }
        packet.setUuid(logInPacket.getUuid());
        out.put(packet);
    }

    @Override
    public synchronized void visit(RoomListPacket roomListPacket) {
        throw new IllegalArgumentException("Server will never receive a room list packet.");
    }

    @Override
    public synchronized void visit(GetRoomListPacket getRoomListPacket) throws InterruptedException {
        out.put(new RoomListPacket(getRoomListPacket.getUsername(), -1, getRoomList(getRoomListPacket.getUsername())));
    }

    private synchronized List<RoomInfo> getRoomList(String username) {
        List<RoomInfo> roomInfoList = new ArrayList<>();
        for (ServerGame<?> game : gameIdToGame.values()) {
            if (game.isEnded()) continue;
            roomInfoList.add(new RoomInfo(game.gameId, game.ownerUsername, game.roomSize, game.joinedPlayers, game.checkIfUserIsInThisGame(username), game.started, usernameToFocusGameId.get(username) == game.gameId));
        }
        return roomInfoList;
    }
    @Override
    public synchronized void visit(EnterRoomPacket enterRoomPacket) throws InterruptedException {
        //TODO
        /**
         * Note:
         * Enter room can have two context:
         * 1. Join a room when you are not in a game yet
         * 2. Switch games
         * So the server should have two hashmaps:
         *  1. map playerId to List<gameId>  (all joined rooms)
         *  2. map playerId to gameId  (current focus)
         *  When user entered a room, we should scan all the games other than this
         *  one that the player just entered, let the out-of-focus player objects all commit!!
         *  Also update the "focus" room hashmap.
         */
        String username = enterRoomPacket.getUsername();
        int gameId = enterRoomPacket.getGameId();
        ServerGame<?> game = gameIdToGame.get(gameId);
        if (game.started && !game.checkIfUserIsInThisGame(username)) {
            out.put(new EnterRoomFailurePacket(username, -1, "ERROR: The game has started, please refresh and choose another game to join."));
            return;
        }
        usernameToFocusGameId.put(username, gameId);
        int playerId;
        System.out.println(username);
        System.out.println(usernameToPlayerIdList.get(username));
        if (!game.checkIfUserIsInThisGame(username)) {
            playerId = playerIdCounter.getAndIncrement();
            usernameToPlayerIdList.get(username).add(playerId);
            playerIdToUsername.put(playerId, username);
            playerIdToGameId.put(playerId, gameId);
        } else {
            playerId = usernameToPlayerIdList.get(username).stream().filter(id -> playerIdToGameId.get(id) == gameId).findFirst().get();
        }
        game.joinGame(username, playerId);
        if (!game.started) {
            out.put(new RoomListPacket(username, -1, getRoomList(username)));
        }
    }

    @Override
    public void visit(GamePacket gamePacket) {
        throw new IllegalArgumentException("Server will nerver receive a game packet.");
    }

    @Override
    public synchronized void visit(DisconnectPacket disconnectPacket) {
        if (!connectionHandlerToUsername.containsKey(disconnectPacket.getUsername()))
            return;
        String username = connectionHandlerToUsername.get(disconnectPacket.getConnectionHandler());
        for (int playerId : usernameToPlayerIdList.get(username)) {
            ServerGame<?> game = gameIdToGame.get(playerIdToGameId.get(playerId));
            game.disconnect(playerId);
        }
        userDB.logOut(username);
    }

    @Override
    public void visit(LogInSuccessPacket logInSuccessPacket) {
        throw new IllegalArgumentException("Server will never receive a login success packet.");
    }

    @Override
    public void visit(SignUpResultPacket signUpFailurePacket) {
        throw new IllegalArgumentException("Server will never receive a sign up result packet.");
    }

    @Override
    public void visit(LogInFailurePacket logInFailurePacket) {
        throw new IllegalArgumentException("Server will never receive a login failure packet.");
    }

    @Override
    public void visit(EnterRoomFailurePacket enterRoomFailurePacket) {
        throw new IllegalArgumentException("Server will never receive a enter room failure packet.");
    }

    @Override
    public synchronized void visit(LogOutPacket logOutPacket) {
        for (int playerId : usernameToPlayerIdList.get(logOutPacket.getUsername())) {
            ServerGame<?> game = gameIdToGame.get(playerIdToGameId.get(playerId));
            game.disconnect(playerId);
        }
        usernameToFocusGameId.put(logOutPacket.getUsername(), -1);
        userDB.logOut(logOutPacket.getUsername());
    }

    @Override
    public synchronized void visit(LeaveGamePacket leaveGamePacket) throws InterruptedException {
        //TODO do we use focusId or playerId from packet
        ServerGame<?> game = gameIdToGame.get(usernameToFocusGameId.get(leaveGamePacket.getUsername()));
        usernameToFocusGameId.put(leaveGamePacket.getUsername(), -1);
        game.disconnect(leaveGamePacket.getPlayerId());
        out.put(new RoomListPacket(leaveGamePacket.getUsername(), -1, getRoomList(leaveGamePacket.getUsername())));
    }


}
