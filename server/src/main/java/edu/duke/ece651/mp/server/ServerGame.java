package edu.duke.ece651.mp.server;

import edu.duke.ece651.mp.common.*;
import edu.duke.ece651.mp.common.packet.*;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;


public class ServerGame<T> {
    final ArrayList<String> colors = new ArrayList<>() {
        {
            add("Green"); add("Red"); add("Blue"); add("Yellow"); add("Purple");
        }
    };
    static final int PLACE = 0;
    static final int MAIN = 1;
    static final int END = 2;
    int gameId;
    String ownerUsername;
    Board<T> board;
    List<Player> players = new ArrayList<>();
    Map<Integer, Player> idToPlayer = new HashMap<>();
    Map<Integer, Boolean> idToActiveStatus = new HashMap<>();
    Map<Integer, Board<T>> idToPrivateBoard = new HashMap<>();
    Executor<T> executor = new ServerExecutor<>();
    BlockingQueue<Packet> out;
    AbstractBoardFactory<?> abf;
    int yetToLosePlayers = 0;
    boolean ended;
    boolean started;
    int gamePhase = 0;
    int roomSize;
    int joinedPlayers = 0;

    public ServerGame(int gameId, String username, int playerId, int size, BlockingQueue<Packet> serverReactionQueue) {
        this.gameId = gameId;
        ownerUsername = username;
        roomSize = size;
        joinedPlayers += 1;
        yetToLosePlayers += 1;
        Player player = new Player(playerId, colors.get(joinedPlayers - 1), 10);
        player.setUsername(username);
        idToPlayer.put(playerId, player);
        idToActiveStatus.put(playerId, true);
        players.add(player);
        abf = new V1BoardFactory(size);
        this.out = serverReactionQueue;
    }

    public void start() throws InterruptedException {
        //TODO
        started = true;
        board = (Board<T>) abf.makeBoard(0, players);
        for (Player p : players) {
            out.put(new PlayerPacket(p.getUsername(), p.getId(), p));
            out.put(new BoardPacket<>(p.getUsername(), p.getId(), board));
            if (idToActiveStatus.get(p.getId()))
                out.put(new PlacePacket(p.getUsername(), p.getId()));
        }
    }

    public synchronized void joinGame(String username, int playerId) throws InterruptedException {
        idToActiveStatus.put(playerId, true);
        if (!checkIfPlayerIsInThisGame(playerId)) {
            //TODO
            joinedPlayers += 1;
            yetToLosePlayers += 1;
            Player player = new Player(playerId, colors.get(joinedPlayers - 1), 10);
            player.setUsername(username);
            idToPlayer.put(playerId, player);
            players.add(player);
            if (joinedPlayers == roomSize) {
                start();
            }
        } else if (started) {
            out.put(new PlayerPacket(username, playerId, idToPlayer.get(playerId)));
            if (idToPrivateBoard.containsKey(playerId)) {
                out.put(new BoardPacket<>(username, playerId, idToPrivateBoard.get(playerId)));
            } else {
                out.put(new BoardPacket<>(username, playerId, board));
                System.out.println("Race condition.");
            }
            if (!idToActiveStatus.get(playerId))
                return;
            if (idToPlayer.get(playerId).loses()) {
                out.put(new WaitPacket(username, playerId, "You have lost, please wait for the result of this turn"));
            } else if (idToPlayer.get(playerId).hasCommitted()) {
                out.put(new WaitPacket(username, playerId, "Please wait until all the other players commit"));
            } else if (gamePhase == PLACE) {
                out.put(new PlacePacket(username, playerId));
            } else {
                out.put(new MainPacket(username, playerId));
            }
        }
        //TODO
    }

    public synchronized void turnEnds() throws InterruptedException {

        this.board = executor.execute(board);// level up technology
        // add one base unit to all territories
        // add tech/food res to each player
        if (gamePhase != PLACE)
            executor.addBaseUnitForAllPlayer(board);
        executor.reset();
        gamePhase = MAIN;
        checkLoseStatusForAll();
        String winnerInfo = checkWinStatusForAll();
        for (Player player : players) {
            if (!player.loses())
                player.resetCommitStatus();
            out.put(new BoardPacket<T>(player.getUsername(), player.getId(), board));
            out.put(new PlayerPacket(player.getUsername(), player.getId(), player));
        }
        if (winnerInfo != null) {
            for (Player player : players) {
                if (idToActiveStatus.get(player.getId()))
                    out.put(new EndPacket(player.getUsername(), player.getId(), winnerInfo));
            }
            ended = true;
            return;
        }
        for (Player player : players) {
            if (!player.loses()) {
                if (idToActiveStatus.get(player.getId()))
                    out.put(new MainPacket(player.getUsername(), player.getId()));
            } else {
                if (idToActiveStatus.get(player.getId()))
                    out.put(new WaitPacket(player.getUsername(), player.getId(), "You have lost, please wait for the result of this turn"));
            }
        }
    }

    public synchronized void receiveAction(Action<T> action) throws InterruptedException {
        if (idToPlayer.get(action.getPlayerId()).loses()) {
            out.put(new WaitPacket(board.getPlayerById(action.getPlayerId()).getUsername(), action.getPlayerId(), "You have lost, no actions permitted."));
            return;
        }
        if (idToPlayer.get(action.getPlayerId()).hasCommitted()) {
            out.put(new WaitPacket(board.getPlayerById(action.getPlayerId()).getUsername(), action.getPlayerId(), "You have already committed."));
            return;
        }
        String errorMsg = executor.tryRecordActionServer(action, board);
        out.put(new CheckResultPacket(board.getPlayerById(action.getPlayerId()).getUsername(), action.getPlayerId(), errorMsg, action));
        if (errorMsg != null)
            return;
        if (checkIfAllCommitted()) {
            turnEnds();
        } else if (!idToPlayer.get(action.getPlayerId()).hasCommitted()) {
            if (gamePhase == PLACE && idToActiveStatus.get(action.getPlayerId())) out.put(new PlacePacket(board.getPlayerById(action.getPlayerId()).getUsername(), action.getPlayerId()));
            if (gamePhase == MAIN && idToActiveStatus.get(action.getPlayerId())) out.put(new MainPacket(board.getPlayerById(action.getPlayerId()).getUsername(), action.getPlayerId()));
        } else {
            if (idToActiveStatus.get(action.getPlayerId())) out.put(new WaitPacket(idToPlayer.get(action.getPlayerId()).getUsername(), action.getPlayerId(), "Please wait until all the players commit."));
        }
    }

    public synchronized void checkLoseStatusForAll() {
        for (Player p : players) {
            if (!p.loses() && p.getTotalUnits() == 0) {
                p.setLoseToTrue();
                --yetToLosePlayers;
            }
        }
    }

    public synchronized boolean isEnded() { return ended; }

    public synchronized String checkWinStatusForAll() {
        if (yetToLosePlayers != 1) return null;
        for (Player p : players) {
            if (p.getTotalUnits() != 0) {
                return "Player " + p.getPlayerColor() + " won!";
            }
        }
        return null;
    }

    public synchronized boolean checkIfAllCommitted() {
        for (Player p : players)
            if (!p.hasCommitted())
                return false;
        return true;
    }

    public synchronized boolean checkIfUserIsInThisGame(String username) {
        for (Player p : players) {
            if (p.getUsername().equals(username)) {
                return true;
            }
        }
        return false;
    }

    private synchronized boolean checkIfPlayerIsInThisGame(int playerId) {
        return idToPlayer.containsKey(playerId);
    }

    public synchronized void disconnect(int playerId) {
        idToActiveStatus.put(playerId, false);
        // idToPlayer.get(playerId).commits();
        //TODO

    }

    public synchronized void storeBoard(int playerId, Board<T> board) {
        idToPrivateBoard.put(playerId, board);
    }
}
