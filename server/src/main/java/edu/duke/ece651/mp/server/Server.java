package edu.duke.ece651.mp.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import edu.duke.ece651.mp.common.ConnectionHandler;
import edu.duke.ece651.mp.common.Player;
import edu.duke.ece651.mp.common.packet.DisconnectPacket;
import edu.duke.ece651.mp.common.packet.MessagePacket;
import edu.duke.ece651.mp.common.packet.Packet;
import edu.duke.ece651.mp.common.packet.PacketHandler;
import org.checkerframework.checker.units.qual.C;

public class Server<T> {

  ServerSocket serverSocket;
  PacketHandler packetHandler;
  ArrayBlockingQueue<Packet> serverReactionQueue;
  ConcurrentHashMap<Integer, ConnectionHandler> uuidToConnectionHandler;
  ConcurrentHashMap<String, ConnectionHandler> usernameToConnectionHandler;
  int uuid;

  public Server(int port) throws IOException,ClassNotFoundException{
    //this will change based on player count
    serverSocket = new ServerSocket(port);
    serverReactionQueue = new ArrayBlockingQueue<>(16384);
    uuidToConnectionHandler = new ConcurrentHashMap<>();
    usernameToConnectionHandler = new ConcurrentHashMap<>();
    uuid = -1;
    packetHandler = new ServerPacketHandler(uuidToConnectionHandler, usernameToConnectionHandler, serverReactionQueue);
  }

  public void receivePacketTask(ConnectionHandler connectionHandler, int uuid) {
    uuidToConnectionHandler.put(uuid, connectionHandler);
    while (true) {
      try {
        Packet packet = connectionHandler.recvPacket();
        packet.setUuid(uuid);
        packet.accept(packetHandler);
      } catch (IOException | ClassNotFoundException | InterruptedException e) {
        new DisconnectPacket("", -1, connectionHandler).accept(packetHandler);
        break;
      } catch (Exception e) {
        e.printStackTrace();

      }
    }
  }
  public void sendPacketTask() {
    ConnectionHandler connectionHandler;
    while (true) {
      connectionHandler = null;
      try {
        Packet packet = serverReactionQueue.take();
        connectionHandler = packet.getUuid() == -1 ? usernameToConnectionHandler.get(packet.getUsername()) : uuidToConnectionHandler.get(packet.getUuid());
        connectionHandler.sendPacket(packet);
      } catch (IOException | InterruptedException e) {
        new DisconnectPacket("", -1, connectionHandler).accept(packetHandler);
      } catch (Exception ignored) {
      }
    }
  }
  
//  public void makeGame() throws IOException, InterruptedException {
//    List<Player> players = new ArrayList<>();
//
//    BlockingQueue<Packet> serverReactionQueue = new ArrayBlockingQueue<>(1024);
//    ServerGame<T> game = new ServerGame<>(players, serverReactionQueue);
//    PacketHandler serverPackerHandler = new ServerPacketHandler(game);
//    Thread sendReactionToClients = new Thread(new Runnable() {
//      @Override
//      public void run() {
//        while (!game.isEnded()) {
//          try {
//            Packet packet = serverReactionQueue.take();
//            conns.get(packet.getPlayerId()).sendPacket(packet);
//          } catch (IOException | InterruptedException e) {
//            return;
//          }
//        }
//      }
//    });
//    sendReactionToClients.start();
//    for (Player player : players) {
//      new Thread(new Runnable() {
//        @Override
//        public void run() {
//          while (!game.isEnded()) {
//            try {
//              Packet packet = conns.get(player.getId()).recvPacket();
//              packet.accept(serverPackerHandler);
//            } catch (IOException | ClassNotFoundException | InterruptedException e) {
//              return;
//            }
//          }
//        }
//      }).start();
//    }
//    game.start();
//    sendReactionToClients.join();
//  }

  public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {

    Server<String> server = new Server<>(12345);
    new Thread(() -> server.sendPacketTask()).start();
    while (true) {
      Socket socket = server.serverSocket.accept();
      server.uuid += 1;
      ConnectionHandler connectionHandler = new ConnectionHandler(socket, "server");
      new Thread(() -> server.receivePacketTask(connectionHandler, server.uuid)).start();
    }


  }
  
}













