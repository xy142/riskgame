package edu.duke.ece651.mp.client;

import edu.duke.ece651.mp.common.Player;
import edu.duke.ece651.mp.common.UpdateLevelAction;
import edu.duke.ece651.mp.common.V1BoardFactory;
import edu.duke.ece651.mp.common.packet.*;
import javafx.application.Platform;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import static org.junit.jupiter.api.Assertions.*;

public class ClientPacketHandlerTest {
@Disabled
  @Test
    public void integratedTest() throws InterruptedException, IOException {
        App app = new App();
        BlockingQueue<Packet> queue = new ArrayBlockingQueue<>(1024);

        ClientGame<String> game = new ClientGame<>(queue);
        app.setClientGame(game);
        game.app = app;
        new Thread(() -> {
            app.main(null);
        }).start();
        Thread.sleep(1000);
        Player p = new Player(0, "red", 10);
        ClientPacketHandler clientPacketHandler = new ClientPacketHandler(game);
        clientPacketHandler.visit(new PlayerPacket("test", 0, new Player(0, "red", 10)));
        clientPacketHandler.visit(new BoardPacket<>("test", 0, new V1BoardFactory(2).makeBoard(0, new ArrayList<>(){{add(p);add(p);}})));

        assertEquals(game.getPlayerId(), 0);
        clientPacketHandler.visit(new MessagePacket("test", 0, "error"));
        clientPacketHandler.visit(new GamePacket("test", 0, new Player(0, "blue", 10), game.getBoard()));
        clientPacketHandler.visit(new EndPacket("test", 0, "ended"));
        assertTrue(game.isEnded());
        assertThrows(IllegalArgumentException.class, () -> clientPacketHandler.visit(new ActionPacket("test", 0, null)));
        clientPacketHandler.visit(new CheckResultPacket("test", 0, null, new UpdateLevelAction<>(p)));
        clientPacketHandler.visit(new PlacePacket("test", 0));
        clientPacketHandler.visit(new MainPacket("test", 0));
        clientPacketHandler.visit(new WaitPacket("test", 0, "wait"));
        assertThrows(IllegalArgumentException.class, () -> clientPacketHandler.visit(new CreateRoomPacket("test", 0, 2)));
        assertThrows(IllegalArgumentException.class, () -> clientPacketHandler.visit(new SignUpPacket("test", 0, "test", "123")));
        clientPacketHandler.visit(new RoomListPacket("test", 0, new ArrayList<>()));
        assertThrows(IllegalArgumentException.class, () -> clientPacketHandler.visit(new GetRoomListPacket("test", 0)));
        assertThrows(IllegalArgumentException.class, () -> clientPacketHandler.visit(new EnterRoomPacket("test", 0, 0)));
        clientPacketHandler.visit(new GamePacket("test", 0, null, null));
        clientPacketHandler.visit(new LogInSuccessPacket("test", 0));
        assertThrows(IllegalArgumentException.class, () -> clientPacketHandler.visit(new DisconnectPacket("test", 0, null)));
        clientPacketHandler.visit(new SignUpResultPacket("test", 0, null));
        clientPacketHandler.visit(new LogInFailurePacket("test", 0, "error"));
        clientPacketHandler.visit(new EnterRoomFailurePacket("Test", 0, "Err"));
        assertThrows(IllegalArgumentException.class, () -> clientPacketHandler.visit(new LogOutPacket("test", 0)));
        assertThrows(IllegalArgumentException.class, () -> clientPacketHandler.visit(new LeaveGamePacket("test", 0)));






    }
}
