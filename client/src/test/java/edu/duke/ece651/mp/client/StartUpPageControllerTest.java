package edu.duke.ece651.mp.client;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyNoInteractions;

import java.io.IOException;

import com.google.common.base.Objects;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.api.FxRobot;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.Start;
import org.testfx.util.WaitForAsyncUtils;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

//@Disabled
@ExtendWith(ApplicationExtension.class)
public class StartUpPageControllerTest {
  private StartUpPageController cont;
  private Button button;
  private Button button1;
  private Label label;
  private TextField user;
  private PasswordField pass;
  private App app;
  private ClientGame CG;
  @Start
  private void Start(Stage stage)throws IOException{
    app = new App();
    CG = mock(ClientGame.class);
    app.clientGame =  CG;
    cont = new StartUpPageController();
    
  }


  @Test
  public void test_signInButtonAction(FxRobot robot)throws IOException, InterruptedException {
Parent SignInView = FXMLLoader.load(getClass().getResource("/xmlFiles/loginScreen.fxml"));
    Scene View = new Scene(SignInView);

    Platform.runLater(()->{
        Button b = new Button();
        try{
          
          cont.signInButtonAction(new ActionEvent(b, null));
        }
        catch(Exception e){
          // e.printStackTrace();
        }
      });
    WaitForAsyncUtils.waitForFxEvents(); 
    verifyNoInteractions(CG);    
    assertNotEquals(app.lastInvokedController, cont);
    assertFalse(Objects.equal(View, cont.SceneView));
    
  }

  @Test
  public void test_signUpButtonAction(FxRobot robot)throws IOException, InterruptedException {
Parent SignupView = FXMLLoader.load(getClass().getResource("/xmlFiles/SignUpScreen.fxml"));
    Scene View = new Scene(SignupView);

    Platform.runLater(()->{
        Button b = new Button();
        try{
          
          cont.signUpButtonAction(new ActionEvent(b, null));
        }
        catch(Exception e){
          // e.printStackTrace();
        }
      });
    WaitForAsyncUtils.waitForFxEvents(); 
    
    verifyNoInteractions(CG);    
    assertNotEquals(app.lastInvokedController, cont);
    assertFalse(Objects.equal(View, cont.SceneView));
  
    
  }
  
}
