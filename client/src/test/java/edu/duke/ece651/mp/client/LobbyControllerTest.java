package edu.duke.ece651.mp.client;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.api.FxRobot;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.Start;
import org.testfx.util.WaitForAsyncUtils;

import edu.duke.ece651.mp.common.RoomInfo;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.stage.Stage;


@ExtendWith(ApplicationExtension.class)

public class LobbyControllerTest {
 private LobbyController cont;
  private Button button;
  private Button button1;
  private Label label;
  private ChoiceBox<String> choice;
  private App app;
  private ClientGame CG;
  private ListView<Button> gameList;


  @Start
  private void Start(Stage stage)throws IOException{
    app = new App();
    CG = mock(ClientGame.class);
    app.clientGame =  CG;
    cont = new LobbyController();
    cont.ErrorLabelLobby = new Label();
    cont.JoinGameButton = button;
    cont.CreateGameButton = button1;
    cont.GamelistBox = new ListView<Button>();
    
  }

  @Disabled
  @Test
  public void test_CreateButtonAction(FxRobot robot)throws  InterruptedException {
    Platform.runLater(()->{
        Button b = new Button();
        try{
          cont.choosePlayersToPlay.getItems().addAll("2");
          cont.choosePlayersToPlay.setValue("2");
          cont.CreateGameButtonAction(new ActionEvent(b, null));
        }
        catch(Exception e){
          // e.printStackTrace();
        }
      });
    WaitForAsyncUtils.waitForFxEvents(); 
    verify(CG).sendCreateRoomRequest(2);
    // verifyNoMoreInteractions(CG);    
    assertNotEquals(app.lastInvokedController, cont);
    }
  
  
  @Test
  public void test_JoinGameButtonAction(FxRobot robot)throws  InterruptedException {
    Platform.runLater(()->{
        Button b = new Button();
        try{
          cont.JoinGameButtonAction(new ActionEvent(b, null));
        }
        catch(Exception e){
          // e.printStackTrace();
        }
      });
    WaitForAsyncUtils.waitForFxEvents();
    verify(CG).sendGetRoomListRequest();
    verifyNoMoreInteractions(CG);    
    assertEquals(app.lastInvokedController, cont);
     }
  
  
  @Test
  public void test_() {
    cont.DisplayMessage("Hello");
    assertEquals("Hello", cont.ErrorLabelLobby.getText());
  }
  
  
  @Test
  public void test_LogOutButtonAction(FxRobot robot)throws IOException,  InterruptedException {
    Parent SignupView = FXMLLoader.load(getClass().getResource("/xmlFiles/loginScreen.fxml"));
    Scene View = new Scene(SignupView);
    Platform.runLater(()->{
        Button b = new Button();
        try{
         
          cont.LogOutButtonAction(new ActionEvent(b, null));
        }
        catch(Exception e){
          e.printStackTrace();
        }
      });
    WaitForAsyncUtils.waitForFxEvents();
    verify(CG).sendLogOutRequest();
    verifyNoMoreInteractions(CG);    
    assertNotEquals(app.lastInvokedController, cont);
    assertNotEquals(cont.SceneView,View);
  }
  
  @Test
  public void test_UpdateGameList(){
    List<RoomInfo> list = new ArrayList<>();
    list.add(new RoomInfo(0, "mac",3, 2, false, false, false));
    list.add(new RoomInfo(1, "eso",2,1, false, false, false));
    cont.UpdateGameList(list);

  }


}
