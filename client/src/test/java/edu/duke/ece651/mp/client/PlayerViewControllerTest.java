package edu.duke.ece651.mp.client;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

import java.io.IOException;

import javax.swing.text.html.ListView;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.api.FxRobot;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.Start;
import org.testfx.util.WaitForAsyncUtils;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.stage.Stage;

@ExtendWith(ApplicationExtension.class)
public class PlayerViewControllerTest {
  private PlayerViewController cont;
  private Button button;
  private Button button1;
  private Label label;
  private ChoiceBox<String> choice;
  private App app;
  private ClientGame CG;
  private ListView gameList;


  @Start
  private void Start(Stage stage)throws IOException{
    app = new App();
    CG = mock(ClientGame.class);
    app.clientGame =  CG;
    cont = new PlayerViewController();
    cont.EndTurnButton = button;
  }

  @Disabled
  @Test
  public void test_EndButtonAction(FxRobot robot)throws  InterruptedException {

    Platform.runLater(()->{
        Button b = new Button();
        try{
          cont.EndTurnButtonAction(new ActionEvent(b, null));
        }
        catch(Exception e){
          // e.printStackTrace();
        }
      });
    WaitForAsyncUtils.waitForFxEvents(); 
    //verify(CG).sendActionRequest(null);;
    //verifyNoMoreInteractions(CG);    
    //assertNotEquals(app.lastInvokedController, cont);
  }
  @Disabled
  @Test
  public void test_DisplayMessage() {
    cont.DisplayMessage("Hello");
    assertEquals("Hello", cont.ErrorLabel.getText());
  }
  

  /*
  @Test
  public void test_JoinGameButtonAction(FxRobot robot)throws  InterruptedException {
    Platform.runLater(()->{
        Button b = new Button();
        try{
          cont.JoinGameButtonAction(new ActionEvent(b, null));
        }
        catch(Exception e){
          // e.printStackTrace();
        }
      });
    WaitForAsyncUtils.waitForFxEvents();
    verify(CG).sendGetRoomListRequest();
    verifyNoMoreInteractions(CG);    
    assertEquals(app.lastInvokedController, cont);
     }
  
  
  
  
  @Test
  public void test_LogOutButtonAction(FxRobot robot)throws IOException,  InterruptedException {
    Parent SignupView = FXMLLoader.load(getClass().getResource("/xmlFiles/loginScreen.fxml"));
    Scene View = new Scene(SignupView);
    Platform.runLater(()->{
        Button b = new Button();
        try{
         
          cont.LogOutButtonAction(new ActionEvent(b, null));
        }
        catch(Exception e){
          e.printStackTrace();
        }
      });
    WaitForAsyncUtils.waitForFxEvents();
    verify(CG).sendLogOutRequest();
    verifyNoMoreInteractions(CG);    
    assertNotEquals(app.lastInvokedController, cont);
    assertNotEquals(cont.SceneView,View);
  }
  
  @Test
  public void test_UpdateGameList(){
    List<RoomInfo> list = new ArrayList<>();
    list.add(new RoomInfo(0, "mac",3, 2, false, false, false));
    list.add(new RoomInfo(1, "eso",2,1, false, false, false));
    cont.UpdateGameList(list);

  }




  @Test
  public void test_() {

  }
  */
}
