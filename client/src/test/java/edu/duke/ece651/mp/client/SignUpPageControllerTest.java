package edu.duke.ece651.mp.client;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.api.FxRobot;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.Start;
import org.testfx.util.WaitForAsyncUtils;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;


@ExtendWith(ApplicationExtension.class)
public class SignUpPageControllerTest {
  private SignUpPageController cont;
  private Button button;
  private Label label;
  private TextField user;
  private PasswordField pass;
  // Model model;
  private App app;
  private ClientGame CG;
  @Start
  private void Start(Stage stage)throws IOException{
    app = new App();
    CG = mock(ClientGame.class);
    app.clientGame =  CG;
    user = new TextField();
    pass = new PasswordField();
    cont = new SignUpPageController();
    cont.username = new TextField();
    cont.password = new PasswordField();
    cont.ErrorLabel = new Label();
    cont.password = pass;
    cont.username = user;
    cont.SubmitButton = button;
    
  }


  @Test
  public void test_SubmitButtonAction(FxRobot robot)throws  InterruptedException {
    Platform.runLater(()->{
        Button b = new Button();
        try{
          cont.username.setText("mac");
          cont.password.setText("mac123");
          cont.SubmitSignUpButtonAction(new ActionEvent(b, null));
        }
        catch(Exception e){
          // e.printStackTrace();
        }
      });
    WaitForAsyncUtils.waitForFxEvents(); 
    verify(CG).sendSignUpRequest("mac", "mac123");
    verifyNoMoreInteractions(CG);    
    assertEquals(app.lastInvokedController, cont);
  }
  
  @Test
  public void test_SubmitButtonActionError(FxRobot robot)throws  InterruptedException {
    Platform.runLater(()->{
        Button b = new Button();
        try{
          cont.username.setText("mac");
          cont.password.setText("");
          cont.SubmitSignUpButtonAction(new ActionEvent(b, null));
        }
        catch(Exception e){
          // e.printStackTrace();
        }
      });
    WaitForAsyncUtils.waitForFxEvents();
    verifyNoInteractions(CG);    
    assertNotEquals(app.lastInvokedController, cont);
    assertEquals("Incomplete Username or Password!", cont.ErrorLabel.getText());
  }
  

  @Test
  public void test_() {
    cont.DisplayMessage("Hello");
    assertEquals("Hello", cont.ErrorLabel.getText());
  }


  @Test
  public void test_BackButtonAction(FxRobot robot)throws IOException,  InterruptedException {
    Parent SignupView = FXMLLoader.load(getClass().getResource("/xmlFiles/StartUpPage.fxml"));
    Scene View = new Scene(SignupView);
    Platform.runLater(()->{
        Button b = new Button();
        try{
         
          cont.BackButtonAction(new ActionEvent(b, null));
        }
        catch(Exception e){
          e.printStackTrace();
        }
      });
    WaitForAsyncUtils.waitForFxEvents(); 
    verifyNoInteractions(CG);    
    assertNotEquals(app.lastInvokedController, cont);
    assertNotEquals(cont.SceneView,View);
  }

}
