package edu.duke.ece651.mp.client;

import edu.duke.ece651.mp.common.AttackAction;
import edu.duke.ece651.mp.common.Player;
import edu.duke.ece651.mp.common.UpdateLevelAction;
import edu.duke.ece651.mp.common.V1BoardFactory;
import edu.duke.ece651.mp.common.packet.*;
import javafx.application.Platform;
import org.checkerframework.checker.units.qual.C;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import javax.sql.rowset.Joinable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import static org.junit.jupiter.api.Assertions.*;

public class ClientGameTest {
  @Disabled
  @Test
    public void integratedTest() throws Exception {
        App app = new App();
        BlockingQueue<Packet> queue = new ArrayBlockingQueue<>(1024);

        ClientGame<String> game = new ClientGame<>(queue);
        app.setClientGame(game);
        game.app = app;
        // Platform.runLater(() -> {app.main(null);});
        Player p = new Player(0, "Red", 10);
        game.initializePlayer(p);
        game.setUsername("Hello");
        game.updateBoard(new V1BoardFactory(2).makeBoard(0, new ArrayList<>(){{add(p);add(p);}}));
        assertEquals(queue.take().getClass(), BoardPacket.class);
        assertEquals(game.getPlayerId(), 0);
        assertEquals(game.getBoard().getPlayerNum(), 2);
        game.sendEnterRoomRequest(0);
        assertEquals(queue.take().getClass(), EnterRoomPacket.class);
        game.sendActionRequest(new UpdateLevelAction<>(p));
        assertEquals(queue.take().getClass(), ActionPacket.class);
        game.sendGetRoomListRequest();
        assertEquals(queue.take().getClass(), GetRoomListPacket.class);
        game.sendLeaveGameRequest();
        assertEquals(queue.take().getClass(), LeaveGamePacket.class);
        game.sendCreateRoomRequest(3);
        assertEquals(queue.take().getClass(), CreateRoomPacket.class);
        game.sendJoinRoomRequest(1);
        assertEquals(queue.take().getClass(), EnterRoomPacket.class);
        game.sendLogInRequest("heelo", "world");
        assertEquals(queue.take().getClass(), LogInPacket.class);
        game.sendLogOutRequest();
        assertEquals(queue.take().getClass(), LogOutPacket.class);
        game.sendSignUpRequest("hello", "world");
        assertEquals(queue.take().getClass(), SignUpPacket.class);
        assertFalse(game.isEnded());
        game.receiveGameData(p, game.getBoard());
        assertFalse(game.getBoard().ifTerritoryExit("durham"));



    }
}
