package edu.duke.ece651.mp.client;

import edu.duke.ece651.mp.common.Action;
import edu.duke.ece651.mp.common.packet.*;

import java.io.IOException;

public class ClientPacketHandler implements PacketHandler {
    private ClientGame<?> game;
    public <T> ClientPacketHandler(ClientGame<T> game) {
        this.game = game;
    }
    @Override
    public <T> void visit(BoardPacket<T> p) throws InterruptedException {
        ((ClientGame<T>)game).updateBoard(p.getBoard());
    }


    @Override
    public void visit(PlayerPacket p) {
        game.initializePlayer(p.getPlayer());
    }

    @Override
    public void visit(ActionPacket p) {
        throw new IllegalArgumentException("Client will never receive an ActionPacket.");
    }

    @Override
    public <T> void visit(CheckResultPacket p) throws InterruptedException {
        game.receiveCheckResult(p.getErrorMsg(), p.getAction());
    }

    @Override
    public void visit(PlacePacket p) throws IOException, InterruptedException {
        game.doPlacementPhase();

    }

    @Override
    public void visit(MainPacket p) throws IOException {
        game.doMainPhase();
    }

    @Override
    public void visit(WaitPacket waitPacket) {
        //TODO
        game.doWaitPhase();
    }

    @Override
    public void visit(EndPacket p) {
        game.finish(p.getWinnerInfo());
    }

    @Override
    public void visit(MessagePacket p) {
//        game.printMessage(p.getMessage());
    }

    @Override
    public void visit(CreateRoomPacket createRoomPacket) {
        throw new IllegalArgumentException("Client will never receive a create room packet.");
    }

    @Override
    public void visit(SignUpPacket signUpPacket) {
        throw new IllegalArgumentException("Client will never receive a sign up packet.");
    }

    @Override
    public void visit(LogInPacket logInPacket) {
        throw new IllegalArgumentException("Client will never receive a login packet.");


    }

    @Override
    public void visit(RoomListPacket roomListPacket) throws IOException {
        //TODO
        game.setUsername(roomListPacket.getUsername());
        game.receiveRoomList(roomListPacket.getRoomInfoList());

    }

    @Override
    public void visit(GetRoomListPacket getWaitingRoomsPacket) {
        throw new IllegalArgumentException("Client will never receive a get room list packet.");
    }

    @Override
    public void visit(EnterRoomPacket enterRoomPacket) {
        throw new IllegalArgumentException("Client will never receive a enter room packet.");
    }

    @Override
    public void visit(GamePacket gamePacket) {
        //TODO
    }

    @Override
    public void visit(DisconnectPacket disconnectPacket) {
        throw new IllegalArgumentException("Client will never receive a disconnect packet.");
    }

    @Override
    public void visit(LogInSuccessPacket logInSuccessPacket) {
        //TODO
        game.setUsername(logInSuccessPacket.getUsername());
    }

    @Override
    public void visit(SignUpResultPacket signUpFailurePacket) throws IOException {
        game.signUpResponse(signUpFailurePacket.getErrorMsg());
    }

    @Override
    public void visit(LogInFailurePacket logInFailurePacket) throws IOException {
        game.logInFailure(logInFailurePacket.getErrorMsg());
    }

    @Override
    public void visit(EnterRoomFailurePacket enterRoomFailurePacket) {
        //TODO
        game.enterRoomFailure(enterRoomFailurePacket.getErrorMsg());
        //throw  new IllegalArgumentException("Client will never receive an enter room packet.");
    }

    @Override
    public void visit(LogOutPacket logOutPacket) {
        throw new IllegalArgumentException("Client will never receive a log out packet");
    }

    @Override
    public void visit(LeaveGamePacket leaveGamePacket) {
        throw new IllegalArgumentException("Client will never receive a leave game packet.");
    }

}
