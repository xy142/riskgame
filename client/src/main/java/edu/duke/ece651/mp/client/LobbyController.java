package edu.duke.ece651.mp.client;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

import edu.duke.ece651.mp.common.RoomInfo;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class LobbyController implements Initializable {

  @FXML
  public Label ErrorLabelLobby;

  @FXML
  private AnchorPane SetupPane;

  @FXML
  public Button LogOutButton;

  @FXML
  ListView<Button> GamelistBox;

  @FXML
  public ChoiceBox<String> choosePlayersToPlay;
  
  @FXML
  public Button CreateGameButton;

  @FXML
  public Button JoinGameButton;
  Scene SceneView;
 
  HashMap<Integer, RoomInfo> gamesAvailable;
  
  public void DisplayMessage(String message){
    ErrorLabelLobby.setText(message);
  }
  //Should be refresh
  public void JoinGameButtonAction(ActionEvent event) throws InterruptedException, IOException {
    //May have to look into this. Server must broadcast every new change to the gmae list
    App.lastInvokedController = this;
    App.clientGame.sendGetRoomListRequest();

  }

  
  public void LogOutButtonAction(ActionEvent event)throws IOException, InterruptedException {
    App.clientGame.sendLogOutRequest();
    Parent SignupView = FXMLLoader.load(getClass().getResource("/xmlFiles/loginScreen.fxml"));
    SceneView = new Scene(SignupView);
    Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
    stage.setScene(SceneView);
    stage.show();
    
  }
  
  public void CreateGameButtonAction(ActionEvent event)throws InterruptedException{
    if(choosePlayersToPlay.getValue() != null ){
      App.lastInvokedController= this;
      App.clientGame.sendCreateRoomRequest(Integer.parseInt(choosePlayersToPlay.getValue()));

    }else{
      
      DisplayMessage("Must select player amount!");
    }
  }
  

  public void UpdateGameList(List<RoomInfo> games) {
    Object that = this;
    GamelistBox.getItems().clear();
    for(RoomInfo room : games){
      String info = "Game Owner = " + room.getRoomOwnerName() +"\n" +"Players needed = " + room.getPlayersNeeded() + "\n" + "Player Joined = "+ room.getPlayersJoined() +"\n" +  "You are playing this game = " + room.areYouInThisRoom() + "\n"+ "Has game started = " + room.isGameStarted();   
      Button button = new Button(info);
      button.setId(String.valueOf(room.getGameId()));
      button.setOnAction(new EventHandler(){
          
        @Override
        public void handle(Event event)  {
          try{
            App.lastInvokedController = that;
          App.clientGame.sendEnterRoomRequest(room.getGameId());
          
          }catch(InterruptedException | IOException e){
            
            System.out.println(e.getMessage());
          }
          
        }});
      
      GamelistBox.getItems().addAll(button);   
    }    
  }
  
  @Override
  public void initialize(URL location, ResourceBundle resources) {
      choosePlayersToPlay.getItems().addAll("2", "3", "4", "5");
    }
    
}


  

