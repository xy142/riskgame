
package edu.duke.ece651.mp.client;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Objects;
import java.util.ResourceBundle;

import edu.duke.ece651.mp.common.Action;
import edu.duke.ece651.mp.common.AttackAction;
import edu.duke.ece651.mp.common.Board;
import edu.duke.ece651.mp.common.CommitAction;
import edu.duke.ece651.mp.common.MoveAction;
import edu.duke.ece651.mp.common.PlaceAction;
import edu.duke.ece651.mp.common.Player;
import edu.duke.ece651.mp.common.Territory;
import edu.duke.ece651.mp.common.Unit;
import edu.duke.ece651.mp.common.UpdateLevelAction;
import edu.duke.ece651.mp.common.UpdateUnitAction;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;


public class PlayerViewController implements Initializable {
  @FXML
  private Button AttackButton;

  @FXML
  private Label AvailFood;

  @FXML
  private Label ColorLabel;

  @FXML
  private ChoiceBox<String> DestTerrChoiceBox;

  @FXML
  private Button DestTerritoryButton;

  @FXML
  private Button ElantrisButton;

  
  @FXML
  public Button EndTurnButton;

  @FXML
  public Label ErrorLabel;

  @FXML
  private Button ExitGameButton;

  @FXML
  private Button FromTerritoryButton;

  @FXML
  private Label GameTitleLabel;

  @FXML
  private Label gamePhaseLabel;

  @FXML
  private Button GondorButton;

  
  @FXML
  private Button HogwartsButton;

  
  @FXML
  private Button MidkemiaButton;

  
  @FXML
  private Button MordorButton;

  
  @FXML
  private Button MoveButton;

  @FXML
  private Button NarniaButton;

  
  @FXML
  private Label NumTechResources;

  @FXML
  private Label NumberofUnits;

  @FXML
  private Button NumofUnitsButton;

  @FXML
  private Label OwnerLabel;

  @FXML
  private Button OzButton;

  
  @FXML
  private Button PlayerCommitButton;

  @FXML
  private Label PlayerNameLabel;

  @FXML
  private Button RosharButton;

  
  @FXML
  private Button ScadrialButton;

  
  @FXML
  private Label PlacementLabel;

  @FXML
  private Label TechLevel;

  @FXML
  private Button UnitTypeButton;

  @FXML
  private ChoiceBox<Integer> UnitTypeChoiceBox;

  @FXML
  private Button UpgradeTechLevel;

  @FXML
  private Button UpgradeUnitsButton;

  @FXML
  private ChoiceBox<String> fromTerrChoiceBox;

  @FXML
  private Label levelFiveLabel;

  @FXML
  private Label levelFourLabel;

  @FXML
  private Label levelZeroLabel;

  @FXML
  private Label levelOneLabel;

  @FXML
  private Label levelSevenLabel;

  @FXML
  private Label levelSixLabel;

  @FXML
  private Label levelThreeLabel;

  @FXML
  private Label levelTwoLabel;

  @FXML
  private Button placementButton;
  
  @FXML
  private TextField unitNumTextField;
  
  @FXML
  private Label territoryFoodProd;

  @FXML
  private Label territoryInfoScreenLabel;

  @FXML
  private Label territorySize;

  @FXML
  private Label territoryTechProd;

  @FXML
  private Label unitsToPlaceLabel;


  private int gamePhase;
  private Board<String> board;
  private Player player;
  private int place = 0;
  private int main = 1;
  private String action;
  private Territory<String> fromTerr;
  private Territory<String> toTerr;
  private HashMap<String, Button> terrioryButtonMap;
  private HashMap<String, Label> levelLabelMap;
  private HashMap<String, Integer > actionMap;

  @Override
  public void initialize(URL location, ResourceBundle resources) {
    App.lastInvokedController = this;
    createMaps();
    
  }
  public void newGameData(Player p1, Board<String> b, int GP, String mes ){
    System.out.println("New Game Data\n");
    this.gamePhase = GP;
    this.board = b;
    this.player = p1;
    DisplayMessage(mes);
    buttonSetup();
    setBoard();
    action = "";
    
    
  }

  //changes color of buttons
  public void setBoard(){
    String color = player.getPlayerColor();
    PlayerNameLabel.setText(player.getUsername());
    ColorLabel.setText(color);
    AvailFood.setText(String.valueOf(player.getFoodRes()));
    TechLevel.setText(String.valueOf(player.getTechLevel()));
    NumTechResources.setText(String.valueOf(player.getTechRes()));
    for (Territory<String> Terr : board.getTerritoryByPlayerId(player.getId())){
      Button button = terrioryButtonMap.get(Terr.getName());
      button.setStyle("-fx-background-color: " + color +";" );
    }
  }
  
  public void loadTerritoryCheckBox(){  
    for (Territory<String> Terr : board.getTerritoryByPlayerId(player.getId())){
      String name = Terr.getName();
      fromTerrChoiceBox.getItems().addAll(name); 
    }
    for (Territory<String> Terr : board.getAllTerritory()){
      String name = Terr.getName();
      DestTerrChoiceBox.getItems().addAll(name);
    }
  }

  public void setTerritories(){
    
    fromTerr = board.getTerritoryByName(fromTerrChoiceBox.getValue());
    toTerr = board.getTerritoryByName(DestTerrChoiceBox.getValue());
  }
  
  

  public void DisplayMessage(String message){
    ErrorLabel.setText(message);
    /*  Alert A = new Alert(Alert.AlertType.INFORMATION);
    A.setContentText(message);
    A.show();*/
  }

  public void buttonSetup(){
    System.out.println("Gamephase from Button setup = " + gamePhase);
    if(gamePhase == place){
      MoveButton.setVisible(false);
      UpgradeTechLevel.setVisible(false);
      UpgradeUnitsButton.setVisible(false);
      AttackButton.setVisible(false);
      DestTerrChoiceBox.setVisible(false);
      DestTerritoryButton.setVisible(false);
      UnitTypeButton.setVisible(false);
      UnitTypeChoiceBox.setVisible(false);
      unitNumTextField.clear();
      unitsToPlaceLabel.setText("Units Available to Place: " + player.getUnitsToPlace());

    }else if (gamePhase == main){
      
      unitNumTextField.clear();
      placementButton.setVisible(false);
      
    }else{
      lockButtons();
    }    
  }
   
  
  public void displayTerritoryInfo(String territory){
    Territory<String> Terr = board.getTerritoryByName(territory);
    OwnerLabel.setText(board.getPlayerById(Terr.getOwnerId()).getUsername());
    NumberofUnits.setText(String.valueOf(Terr.getUnits().getTotalUnits()));
    territoryInfoScreenLabel.setText(territory);
    territorySize.setText(String.valueOf(Terr.getSize()));
    territoryFoodProd.setText(String.valueOf(Terr.getFoodProduction()));
    territoryTechProd.setText(String.valueOf(Terr.getTechProduction()));
    int highestLvl = Terr.getUnits().getHighestLevel();
   

    for(int i  = 0; i <  highestLvl; i++){
      levelLabelMap.get(String.valueOf(i)).setText(String.valueOf(Terr.getUnitsByLevel(i)));
    }   
    
  }

  
  public void resetChoiceBox(){
    fromTerrChoiceBox.getItems().clear();
    DestTerrChoiceBox.getItems().clear();
    UnitTypeChoiceBox.getItems().clear();
  }

  
  @FXML
  void PlayerCommitButtonAction(ActionEvent event)throws IOException, InterruptedException {
    if(Objects.equals(action, "")){
      DisplayMessage("Error: Select an Action Before Commiting");
      return;
    }  sendOrders(action);  
  }
  

  private void sendOrders(String order)throws IOException, InterruptedException{
    try{
      setTerritories();
      
      Unit units = new Unit();
      
      
      if(unitNumTextField.getText() == null){
        throw new Exception("Error: Input the amount of units for the Action!!");
      }else if (Integer.parseInt(unitNumTextField.getText()) < 0){
        throw new Exception("Error: Value must be greater than 0!!");
      }
    
      int  unitToOrder= Integer.parseInt(unitNumTextField.getText());
    
      if(Objects.equals(order, "place")){
        units.setUnitsByLevel(0, unitToOrder);       
      }else{
        
        units.setUnitsByLevel(UnitTypeChoiceBox.getValue(), unitToOrder);   
      }
      
      if(Objects.equals(order, "place")){
        
        App.clientGame.sendActionRequest(new PlaceAction<String>(player, fromTerr, fromTerr, units));
      }
      else if (Objects.equals(order, "attack")){
   
        App.clientGame.sendActionRequest(new AttackAction<>(player, fromTerr, toTerr, units));
      }
      else if (Objects.equals(order, "move")){      
        
        App.clientGame.sendActionRequest(new MoveAction<>(player, fromTerr, toTerr, units));
      }
      else if (order == "UnitUpgrade"){
        App.clientGame.sendActionRequest(new UpdateUnitAction<>(player, fromTerr, units));
      }
    }catch(NullPointerException e){
      DisplayMessage("Error:One choiceBox is empty!");

    }catch(NumberFormatException e){
      DisplayMessage("Error:Invalid integer in unit amount field!");
  
    }catch(Exception e){
      DisplayMessage(e.getMessage());
     
    }
  }
    
  @FXML
  void UpgradeUnitsButtonAction(ActionEvent event) {
    resetChoiceBox();
    loadTerritoryCheckBox();
    DestTerritoryButton.setText("");
    FromTerritoryButton.setText("UPGR-FROM");
    action = "UnitUpgrade";
  }

  
  
  @FXML
  void AttackButtonAction(ActionEvent event) {
      resetChoiceBox();
      loadTerritoryCheckBox();
      DestTerritoryButton.setText("ATK-TO");
      FromTerritoryButton.setText("ATK-FROM");
      action = "attack";
    }

    @FXML
      void MoveButtonAction(ActionEvent event) {
      resetChoiceBox();
      loadTerritoryCheckBox();
      DestTerritoryButton.setText("MV-TO");
      FromTerritoryButton.setText("MV-FROM");
      action = "move";
    }
    @FXML
      void placementActionButton(ActionEvent event) {
      resetChoiceBox();
      loadTerritoryCheckBox();
      FromTerritoryButton.setText("ToPlace");
      action = "place";
    }

  
    @FXML
      void EndTurnButtonAction(ActionEvent event)throws IOException, InterruptedException {
   
      App.clientGame.sendActionRequest(new CommitAction<>(player));
    }

    @FXML
      void unitTypeAction(MouseEvent event) {
      unitNumTextField.clear();//was giving us problems before
      UnitTypeChoiceBox.getItems().clear();
      if(fromTerrChoiceBox.getValue() == null || action == null || action == "place"){
        DisplayMessage("Error:Must have from Territory selected or you need to select an Action!");
        return;
      }
      for(int i = 0; i < 7; i++){
        int level = board.getTerritoryByName(fromTerrChoiceBox.getValue()).getUnitsByLevel(i);
        if (0 != level){
          System.out.println("Unit levels = " + i);
          UnitTypeChoiceBox.getItems().addAll(i);
        }
    }
  }

  @FXML
  void ExitGameButtonAction(ActionEvent event) throws IOException, InterruptedException {
    App.clientGame.sendLeaveGameRequest();//////Verify
    Parent LobbyView = FXMLLoader.load(getClass().getResource("/xmlFiles/LobbyPage.fxml"));
    Scene SceneView = new Scene(LobbyView);
    Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
    stage.setScene(SceneView);
    stage.show();

  }
  @FXML
  void territoryInfoAction(ActionEvent event) {

    displayTerritoryInfo(((Button)event.getSource()).getText());
  }


  @FXML
  void UpgradeTechLevelAction(ActionEvent event)throws InterruptedException {
    App.clientGame.sendActionRequest(new UpdateLevelAction<String>(player));
  }

  
  

  public void lockButtons(){
    fromTerrChoiceBox.setVisible(false);
    DestTerrChoiceBox.setVisible(false);
    unitNumTextField.setVisible(false);
    UnitTypeChoiceBox.setVisible(false);
    MoveButton.setVisible(false);
    AttackButton.setVisible(false);
    placementButton.setVisible(false);
    UpgradeUnitsButton.setVisible(false);
    UpgradeTechLevel.setVisible(false);
    PlayerCommitButton.setVisible(false);
    EndTurnButton.setVisible(false);
    FromTerritoryButton.setVisible(false);
    DestTerritoryButton.setVisible(false);
    UnitTypeButton.setVisible(false);
    NumofUnitsButton.setVisible(false);
  }
  
  public void unlockButtons(){
    fromTerrChoiceBox.setVisible(true);
    DestTerrChoiceBox.setVisible(true);
    unitNumTextField.setVisible(true);
    UnitTypeChoiceBox.setVisible(true);
    MoveButton.setVisible(true);
    AttackButton.setVisible(true);
    placementButton.setVisible(true);
    UpgradeUnitsButton.setVisible(true);
    UpgradeTechLevel.setVisible(true);
    PlayerCommitButton.setVisible(true);
    EndTurnButton.setVisible(true);
     FromTerritoryButton.setVisible(true);
    DestTerritoryButton.setVisible(true);
    UnitTypeButton.setVisible(true);
    NumofUnitsButton.setVisible(true);
    
  }
  private void createMaps(){
    terrioryButtonMap = new HashMap<>();
    levelLabelMap = new HashMap<>();
    actionMap = new HashMap<>();
    terrioryButtonMap.put("Narnia", NarniaButton);
    terrioryButtonMap.put("Roshar", RosharButton);
    terrioryButtonMap.put("Oz", OzButton);
    terrioryButtonMap.put("Midkemia", MidkemiaButton);
    terrioryButtonMap.put("Scadrial", ScadrialButton);
    terrioryButtonMap.put("Elantris", ElantrisButton);
    terrioryButtonMap.put("Mordor", MordorButton);
    terrioryButtonMap.put("Gondor", GondorButton);
    terrioryButtonMap.put("Hogwarts", HogwartsButton);

    levelLabelMap.put("0", levelZeroLabel);
    levelLabelMap.put("1", levelOneLabel);
    levelLabelMap.put("2", levelTwoLabel);
    levelLabelMap.put("3", levelThreeLabel);
    levelLabelMap.put("4", levelFourLabel);
    levelLabelMap.put("5", levelFiveLabel);
    levelLabelMap.put("6", levelSixLabel);
    levelLabelMap.put("7", levelSevenLabel);

    actionMap.put("place", 0);
    actionMap.put("move", 1);
    actionMap.put("place", 3);
    actionMap.put("UpgradeUnit", 5);
                  }
  @FXML
  void NumofUnitsButtonAction(ActionEvent event) {

  }

  @FXML
  void UnitTypeButtonAction(ActionEvent event) {
    
  }

  @FXML
  void FromTerritoryButtonAction(ActionEvent event) {

  }

  @FXML
  void DestTerritoryButtonAction(ActionEvent event) {

  }
}
