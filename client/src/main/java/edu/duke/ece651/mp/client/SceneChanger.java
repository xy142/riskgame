package edu.duke.ece651.mp.client;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

import edu.duke.ece651.mp.common.RoomInfo;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class SceneChanger implements Initializable {
  public Stage stage;
  public HashMap<String, Scene> scenes;
  private int resX;
  private int resY;
  public Object lastInvokedController = null;
  public List<RoomInfo> games;

  public SceneChanger(Stage stage, int resX, int resY) throws IOException{
    this.scenes = new HashMap<>();
    this.games = new ArrayList<>();
    this.resX = resX;
    this.resY = resY;
    this.stage = stage;
    this.scenes.put("loginScreen", this.createLoginScreen()); 
    this.scenes.put("lobbyScreen", this.createLobbyScreen());
    //    this.scenes.put("playerScreen", this.createPlayerScreen());
 
  }
  
  @Override
  public void initialize(URL location, ResourceBundle resources) {
    
  }

  public Scene createLoginScreen()throws IOException{
    FXMLLoader loader;
    loader = new FXMLLoader(getClass().getResource("/xmlFiles/loginScreen.fxml"));
    Parent root = loader.load();   
    return  new Scene(root, resX, resY);      
  
  }
  /*  public Scene createPlayerScreen()throws IOException{
    System.out.println("createPlayer\n");
    FXMLLoader loader;
    loader = new FXMLLoader(getClass().getResource("/xmlFiles/PlayerView.fxml"));
    Parent root = loader.load();
    return  new Scene(root, resX, resY);      
  
    }*/
  public Scene createLobbyScreen()throws IOException{    
    FXMLLoader loader;
    loader = new FXMLLoader(getClass().getResource("/xmlFiles/LobbyPage.fxml"));
    Parent root = loader.load();
    LobbyController control = loader.getController();
    control.UpdateGameList(games);
    return  new Scene(root, resX, resY);
    
    }
  
  public  void showView(String sceneName){
    Scene SceneView = scenes.get(sceneName);
    stage.setScene(SceneView);
    stage.show();
  }


}
