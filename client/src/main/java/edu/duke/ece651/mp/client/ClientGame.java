package edu.duke.ece651.mp.client;
import edu.duke.ece651.mp.common.*;
import edu.duke.ece651.mp.common.packet.*;
import javafx.application.Platform;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ClientGame<T> {
  private Executor<T> executor;
  private BufferedReader inputReader;
  String username;
  private PrintStream out;
  private Player player;
  private Board<T> board;
  private BlockingQueue<Packet> clientReactionQueue;
  private boolean ended;
  App app;
  public ClientGame(){
    this.clientReactionQueue = new ArrayBlockingQueue<>(100);

  }
  public ClientGame(BlockingQueue<Packet> clientReactionQueue) {
    executor = new ClientExecutor<>();
    this.clientReactionQueue = clientReactionQueue;
  }

  public void initializePlayer(Player player) {
    this.player = player;
  }
  public void setUsername(String username) { this.username = username; }
  public void updateBoard(Board<T> board) throws InterruptedException {
    this.board = board;
    player.resetCommitStatus();
    clientReactionQueue.put(new BoardPacket<>(username, player.getId(), board));
  }

  public Board<T> getBoard(){
    return board;
  }
  public int getPlayerId(){
    return player.getId();
  }

  public void sendLogInRequest(String username, String password) throws InterruptedException {
    clientReactionQueue.put(new LogInPacket("",-1, username, password));
  }

  public void sendLogOutRequest() throws InterruptedException {
    clientReactionQueue.put(new LogOutPacket(username, -1));
  }

  public void sendSignUpRequest(String username, String password) throws InterruptedException {
    clientReactionQueue.put(new SignUpPacket("", -1, username, password));
  }

  public void sendActionRequest(Action<T> action) throws InterruptedException {
    clientReactionQueue.put(new ActionPacket(username, player.getId(), action));
  }

  public void sendCreateRoomRequest(int roomSize) throws InterruptedException {
    clientReactionQueue.put(new CreateRoomPacket(username, -1, roomSize));
  }

  public void sendJoinRoomRequest(int gameId) throws InterruptedException {
    clientReactionQueue.put(new EnterRoomPacket(username, -1, gameId));
  }

    public void sendLeaveGameRequest() throws InterruptedException {
        clientReactionQueue.put(new LeaveGamePacket(username, player.getId()));
    }


  public void sendGetRoomListRequest() throws InterruptedException {
    clientReactionQueue.put(new GetRoomListPacket(username, -1));
  }


  public void sendEnterRoomRequest(int gameId) throws InterruptedException, IOException {
    clientReactionQueue.put(new EnterRoomPacket(username, -1, gameId));
    /*//Testing Only
    V1BoardFactory factory = new V1BoardFactory(2);
    ArrayList<Player> playerList = new ArrayList<>();
    Player p1 = new Player(0, "Blue", 10);
    playerList.add(p1);
    playerList.add(new Player(1,"Yello",10));
    Board<String> b = factory.makeBoard(0,playerList);   
    app.receiveGameData(p1, b, 0, null);*/
        
    //app.openGameScene();
  }

  public void logInFailure(String errorMsg) throws IOException {
    Platform.runLater(() ->{
        try {
          app.LogInFailure(errorMsg);
        } catch (IOException e) {
          e.printStackTrace();
        }
      });
  }

  public void signUpResponse(String errorMsg) throws IOException {
    Platform.runLater(() -> {
        try {
          app.SignUpResponse(errorMsg);
        } catch (IOException e) {
          e.printStackTrace();
        }
      });


  }


  public void receiveGameData(Player player, Board<T> board) {
    this.player = player;
    this.board = board;
  }

  public void doPlacementPhase()  {
    //TODO
    //Possible Error, player not the board's player
    Platform.runLater(() ->{
        try{
          app.receiveGameData(player, (Board<String>) board, 0, "Please choose a territory and place units on it.");
        }catch(IOException e){
            
          System.out.println(e.getMessage());  
        }
      });

  }

  public void doMainPhase()throws IOException {
    //TODO
    //disable and enable buttons
    Platform.runLater(() -> {
        try{
          app.receiveGameData(player, (Board<String>) board, 1, "Please enter an action or end turn.");
        }catch(IOException e){
            
          System.out.println(e.getMessage());  
        }
      });
  }



  public void doWaitPhase() {
    Platform.runLater(() -> {
        try{
          app.receiveGameData(player, (Board<String>) board, 2, "Please wait until all the other players commit.");
        }catch(IOException e){
            
          System.out.println(e.getMessage());  
        }
      });

  }
  public void finish(String winnerInfo) {
    Platform.runLater(() ->{
        try{
          app.receiveGameData(player, (Board<String>) board, 3, winnerInfo);
        }catch(IOException e){
            
          System.out.println(e.getMessage());  
        }
      });
  }

  public boolean isEnded() {
    return ended;
  }

  public void receiveCheckResult(String errorMsg, Action<T> action) throws InterruptedException {
    if (errorMsg == null) {
      executor.tryRecordActionClient(action, board); // note no-store, so reset
      player = board.getPlayerById(player.getId());
      clientReactionQueue.put(new BoardPacket<T>(username, player.getId(), board));
    } else {
      Platform.runLater(() ->{
          try {
            app.invalidAction(errorMsg);
          } catch (IOException e) {
            e.printStackTrace();
          }
        });
    }
  }

    public void receiveRoomList(List<RoomInfo> roomInfoList) throws IOException {
        //TODO
        Platform.runLater(() -> {
            try {
                app.returnToLobby2(roomInfoList);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void enterRoomFailure(String errorMsg) {
        // TODO
        // create method in App
        // in button, that = this
        Platform.runLater(() -> app.joinGameFailure(errorMsg));
    }

//    private int readUnits(String prompt) throws IOException {
//        out.println(prompt);
//        String integerString;
//        while (true) {
//            integerString = inputReader.readLine();
//            int units;
//            try {
//                units = Integer.parseInt(integerString);
//            } catch (NumberFormatException e) {
//                out.println("Your input is not an integer, please enter again!");
//                continue;
//            }
//            if (units <= 0) {
//                out.println("Please enter a positive number.");
//                continue;
//            }
//            return units;
//        }
//    }



}











