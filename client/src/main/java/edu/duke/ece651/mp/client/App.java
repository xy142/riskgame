package edu.duke.ece651.mp.client;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import edu.duke.ece651.mp.common.Board;
import edu.duke.ece651.mp.common.Player;
import edu.duke.ece651.mp.common.RoomInfo;
import edu.duke.ece651.mp.common.V1BoardFactory;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;




public class App extends Application {
  static ClientGame<String> clientGame;
  static Object lastInvokedController = null;
  private static SceneChanger sceneChanger;
  private static Stage stage;
  public static Stage stage2  = stage;
  public void setClientGame(ClientGame<String> clientgame){
    App.clientGame = clientgame;
  }

  public void  SignUpResponse(String message)throws IOException{
    if(message == null){;
      sceneChanger.showView("loginScreen");
    }else{
      SignUpPageController Control = (SignUpPageController) lastInvokedController;
      Control.DisplayMessage(message);
    }    
  }

  public void LogInFailure(String message)throws IOException{
    LoginPageController Control = (LoginPageController) lastInvokedController;
    Control.DisplayMessage(message);    
  }

   public void openGameScene()throws IOException{
     System.out.println("Open Scene\n");
      sceneChanger.showView("playerScreen");
    }
          
  
   public void joinGameFailure(String errorMsg) {
    LobbyController controller = (LobbyController) App.lastInvokedController;
    controller.DisplayMessage(errorMsg);
   }

  public void returnToLobby2(List<RoomInfo> games) throws IOException {
    FXMLLoader loader;
    loader = new FXMLLoader(getClass().getResource("/xmlFiles/LobbyPage.fxml"));
    Parent root = loader.load();
    LobbyController control = loader.getController();
    control.UpdateGameList(games);
    Scene SceneView = new Scene(root);
    stage.setScene(SceneView);
    stage.show();

  }

  
  public void receiveGameData(Player player, Board<String> board, int state, String message)throws IOException{
    System.out.println("receive Game Data\n");
    FXMLLoader loader;
    loader = new FXMLLoader(getClass().getResource("/xmlFiles/PlayerView.fxml"));
    Parent root = loader.load();
    PlayerViewController control = loader.getController();
    control.newGameData(player, board, state, message);
    Scene SceneView = new Scene(root);
    stage.setScene(SceneView);
    stage.show();
    System.out.println("Hello from the bottom!\n");
   

    
  }


  public void invalidAction(String message)throws IOException{

    PlayerViewController Control = (PlayerViewController) lastInvokedController;
     Control.DisplayMessage(message);
    

  }

   

  @Override
  public void start(Stage stage)throws IOException {
    App.stage = stage;
    sceneChanger = new SceneChanger(App.stage, 1300, 600);
    URL xmlResource = getClass().getResource("/xmlFiles/StartUpPage.fxml");
    Parent gp = FXMLLoader.load(xmlResource);
    Scene scene = new Scene(gp, 1300, 600);
    stage.setTitle("RISC");
    stage.setScene(scene);
    stage.show();
  }

  public static void main(String[] args){
    launch();
  }

}
