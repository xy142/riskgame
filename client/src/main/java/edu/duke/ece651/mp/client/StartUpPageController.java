package edu.duke.ece651.mp.client;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class StartUpPageController implements Initializable {
  @FXML
  public Button signInButton;
  @FXML
  public Button signUpBottom;

  Scene SceneView;

  public void signInButtonAction(ActionEvent event) throws Exception{
    Parent SigninView = FXMLLoader.load(getClass().getResource("/xmlFiles/loginScreen.fxml"));
    SceneView = new Scene(SigninView);
    Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
    window.setScene(SceneView);
    window.show();
  }

  public void signUpButtonAction(ActionEvent event) throws Exception{
    Parent SignupView = FXMLLoader.load(getClass().getResource("/xmlFiles/SignUpScreen.fxml"));
    SceneView = new Scene(SignupView);
    Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
    window.setScene(SceneView);
    window.show();
  }
  

  @Override
  public void initialize(URL location, ResourceBundle resources) {
    // TODO Auto-generated method stub
    
  }

}
