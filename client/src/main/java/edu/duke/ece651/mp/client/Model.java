package edu.duke.ece651.mp.client;

import java.util.HashMap;

public class Model {
    public String getColor(String terrtoryName) {
        return "white";
    }
    
    public String getResourcesByName(String terrtoryName) {
        return "food";
    }

    public String getCurrentTechLevel() {
        return "2";
    }

    public int getTotalResources() {
        return 0;
    }

    public int getGameNumber() {
        return 0;
    }

    public int getResourcesPerTurn() {
        return 0;
    }

    public HashMap<Integer, Integer> getUnitsByName(String territoryName) {
        HashMap<Integer, Integer> result = new HashMap<>();
        result.put(1, 10);
        result.put(2, 1);
        return result;
    }
}
