package edu.duke.ece651.mp.client;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class SignUpPageController implements Initializable {

  @FXML
  TextField username;

  @FXML
  PasswordField password;

  @FXML
  Button SubmitButton;

  @FXML
  Label ErrorLabel;

  @FXML
  Button BackButton;
  Scene SceneView;
  public void DisplayMessage(String message){
    ErrorLabel.setText(message);
  }
 
  public void BackButtonAction(ActionEvent event) throws IOException{
    Parent SignupView = FXMLLoader.load(getClass().getResource("/xmlFiles/StartUpPage.fxml"));
    SceneView = new Scene(SignupView);
    Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
    stage.setScene(SceneView);
    stage.show();
  }
  
  public void SubmitSignUpButtonAction(ActionEvent event)throws IOException, InterruptedException{
    if(!this.username.getText().isEmpty() && !this.password.getText().isEmpty()){
       App.lastInvokedController = this;
      App.clientGame.sendSignUpRequest(this.username.getText(),this.password.getText());

    }else{

      ErrorLabel.setText("Incomplete Username or Password!");
    }


  }

  @Override
  public void initialize(URL location, ResourceBundle resources) {
  }

  

}
