package edu.duke.ece651.mp.common;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;


import org.junit.jupiter.api.Test;

public class V1BoardFactoryTest {
    @Test
    public void test_constructor(){
        assertThrows(IllegalArgumentException.class, ()->new V1BoardFactory(1));
        V1BoardFactory factory = new V1BoardFactory(2);
        BoardModel<String> model = new V1BoardModelDefault(2);
        assertEquals(model.getPlayerterris(), factory.boardModelList.get(0).getPlayerterris());
        assertEquals(model.getterris(), factory.boardModelList.get(0).getterris());
        assertEquals(model.getneighs(), factory.boardModelList.get(0).getneighs());

    }
    @Test
    public void test_makeBoard() {
        V1BoardFactory factory = new V1BoardFactory(2);
        ArrayList<Player> playerList = new ArrayList<>();
        playerList.add(new Player(0,"Blue",0));
        playerList.add(new Player(1,"Yello",0));
        Board<String> b = factory.makeBoard(0,playerList);
        assertEquals( b.getTerritoryByName("Narnia").getOwnerId(),0);
        assertEquals( b.getTerritoryByName("Midkemia").getOwnerId(),0);
        assertEquals( b.getTerritoryByName("Oz").getOwnerId(),0);
        assertEquals( b.getTerritoryByName("Gondor").getOwnerId(),0);
        assertEquals( b.getTerritoryByName("Elantris").getOwnerId(),1);
        assertEquals( b.getTerritoryByName("Scadrial").getOwnerId(),1);
        assertEquals( b.getTerritoryByName("Roshar").getOwnerId(),1);
        assertEquals( b.getTerritoryByName("Mordor").getOwnerId(),1);
        assertEquals( b.getTerritoryByName("Hogwarts").getOwnerId(),1);
    }
}
