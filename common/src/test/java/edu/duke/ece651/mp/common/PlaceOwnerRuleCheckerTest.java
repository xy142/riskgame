package edu.duke.ece651.mp.common;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

public class PlaceOwnerRuleCheckerTest {
  @Test
  public void test_() {
     ArrayList<Player> playerlist = new ArrayList<>();
    playerlist.add(new Player(0, "GREEN", 10));
    playerlist.add(new Player(1, "BLACK", 10));
    playerlist.add(new Player(2, "RED", 10));
    playerlist.add(new Player(3, "YELLOW", 10));
    
    V1BoardFactory BF = new V1BoardFactory(4);
    Board<String> B = BF.makeBoard(0, playerlist);

    RuleChecker<String> check = new PlaceOwnerRuleChecker<String>(null);
    Unit u = new Unit();
    u.setUnitsByLevel(0, 5);
    Action<String> A1 = new PlaceAction<>(playerlist.get(0), B.getTerritoryByName("Narnia"),B.getTerritoryByName("Midkemia"), u);
    assertEquals(null, check.checkMyRule(A1, B));
    
     Action<String> A2 = new PlaceAction<>(playerlist.get(1), B.getTerritoryByName("Gondor"),B.getTerritoryByName("Narnia"), u);
    assertEquals("You don't own Narnia.", check.checkMyRule(A2, B));    
    
     

  }

}
