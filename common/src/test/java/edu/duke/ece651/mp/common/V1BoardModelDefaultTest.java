package edu.duke.ece651.mp.common;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.jupiter.api.Test;

public class V1BoardModelDefaultTest {
    @Test
    public void test_getteris() {
        V1BoardModelDefault model = new V1BoardModelDefault(2);
         HashMap<String, Territory<String>> modelTerris = new HashMap<>() {
            {
                put("Oz", new V1Territory("Oz"));
                put("Narnia", new V1Territory("Narnia"));
                put("Midkemia", new V1Territory("Midkemia"));
                put("Gondor", new V1Territory("Gondor"));
                put("Elantris", new V1Territory("Elantris"));
                put("Scadrial", new V1Territory("Scadrial"));
                put("Mordor", new V1Territory("Mordor"));
                put("Roshar", new V1Territory("Roshar"));
                put("Hogwarts", new V1Territory("Hogwarts"));
    
            }
        };
        assertEquals(modelTerris, model.getterris());
    }
    @Test
    public void test_getneighs(){
        V1BoardModelDefault model = new V1BoardModelDefault(2);
        ArrayList<String> neighsOfOz = model.getneighs().get("Oz");
        ArrayList<String> expected = new ArrayList<>();
        expected.add("Midkemia");
        expected.add("Scadrial");
        expected.add("Mordor");
        expected.add("Gondor");
        assertEquals(expected,neighsOfOz);

    }

    @Test
    public void test_getteris_getneighs_getPlayerterris() {
        //2player
        V1BoardModelDefault model = new V1BoardModelDefault(2);
        ArrayList<String> expected = new ArrayList<>(List.of("Oz","Midkemia","Narnia","Gondor"));
        assertEquals(expected, model.getPlayerterris().get(0));
        //3player
        model = new V1BoardModelDefault(3);
        expected = new ArrayList<>(List.of("Oz","Midkemia","Narnia"));
        assertEquals(expected, model.getPlayerterris().get(0));
        //4player
        model = new V1BoardModelDefault(4);
        expected = new ArrayList<>(List.of("Midkemia","Narnia"));
        assertEquals(expected, model.getPlayerterris().get(0));
        //5player
        model = new V1BoardModelDefault(5);
        expected = new ArrayList<>(List.of("Elantris"));
        assertEquals(expected, model.getPlayerterris().get(4));
        //1 player
        assertThrows(IllegalArgumentException.class, ()->new V1BoardFactory(1));
        //6 player
        assertThrows(IllegalArgumentException.class, ()->new V1BoardFactory(6));

    }


}
