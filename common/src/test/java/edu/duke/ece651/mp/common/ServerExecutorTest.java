package edu.duke.ece651.mp.common;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

public class ServerExecutorTest {
  @Test
  public void test_() {

    ArrayList<Player> playerlist = new ArrayList<>();
    playerlist.add(new Player(0, "GREEN", 10));
    playerlist.add(new Player(1, "BLACK", 10));
    playerlist.add(new Player(2, "RED", 10));
    playerlist.add(new Player(3, "YELLOW", 10));

    V1BoardFactory BF = new V1BoardFactory(4);
    Board<String> B = BF.makeBoard(0, playerlist);
    Executor<String> E = new ServerExecutor<String>();

    RuleChecker<String> Attack = new AttackOwnerRuleChecker<>(new AdjacentRuleChecker<>(new SufficientUnitsRuleChecker<>(null)));


    Unit u = new Unit();
    u.setUnitsByLevel(0, 5);
    Action<String> A1 = new PlaceAction<>(playerlist.get(0), B.getTerritoryByName("Narnia"),B.getTerritoryByName("Midkemia"), u);

    assertEquals(null,E.tryRecordActionServer(A1, B));
    u.setUnitsByLevel(0, 0);
     Action<String> A2 = new AttackAction<>(playerlist.get(1), B.getTerritoryByName("Gondor"),B.getTerritoryByName("Narnia"), u);
     assertEquals("Gondor and Narnia are not adjacent.",E.tryRecordActionServer(A2, B));
    Action<String> A3 = new MoveAction<>(playerlist.get(2), B.getTerritoryByName("Mordor"),B.getTerritoryByName("Hogwarts"), u);
    // assertEquals(null,E.tryRecordActionServer(A3, B));
    Action<String> A4 = new CommitAction<>(playerlist.get(3), B.getTerritoryByName("Mordor"),B.getTerritoryByName("Hogwarts"), u);

    //assertEquals(null,E.tryRecordAction(A4, B));
     //assertEquals(Attack, E.getRuleCheckerByType(0));


  }

  @Test
  public void test_1() {

    ArrayList<Player> playerlist = new ArrayList<>();
    playerlist.add(new Player(0, "GREEN", 10));
    playerlist.add(new Player(1, "BLACK", 10));
    playerlist.add(new Player(2, "RED", 10));
    playerlist.add(new Player(3, "YELLOW", 10));

    V1BoardFactory BF = new V1BoardFactory(4);
    Board<String> B = BF.makeBoard(0, playerlist);
    Executor<String> E = new ServerExecutor<String>();

    RuleChecker<String> Attack = new AttackOwnerRuleChecker<>(new AdjacentRuleChecker<>(new SufficientUnitsRuleChecker<>(null)));
    Unit u = new Unit();
    u.setUnitsByLevel(0, 0);
    Action<String> A1 = new AttackAction<>(playerlist.get(0), B.getTerritoryByName("Narnia"),B.getTerritoryByName("Elantris"), u);

    assertEquals(null,E.tryRecordActionServer(A1, B));

    /* Action<String> A2 = new Action<>(playerlist.get(1), 3, B.getTerritoryByName("Gondor"),B.getTerritoryByName("Narnia"), 0);
     assertEquals("Gondor and Narnia are not adjacent.",E.tryRecordAction(A2, B));
     Action<String> A3 = new Action<>(playerlist.get(2), 1, B.getTerritoryByName("Mordor"),B.getTerritoryByName("Hogwarts"), 0);
     assertEquals(null,E.tryRecordAction(A3, B));
     Action<String> A4 = new Action<>(playerlist.get(3), 3, B.getTerritoryByName("Mordor"),B.getTerritoryByName("Hogwarts"), 0);*/

     //assertEquals(null,E.tryRecordAction(A4, B));
     //assertEquals(Attack, E.getRuleCheckerByType(0));


  }

  @Test
  public void test_2() {

    ArrayList<Player> playerlist = new ArrayList<>();
    playerlist.add(new Player(0, "GREEN", 10));
    playerlist.add(new Player(1, "BLACK", 10));
    playerlist.add(new Player(2, "RED", 10));
    playerlist.add(new Player(3, "YELLOW", 10));

    V1BoardFactory BF = new V1BoardFactory(4);
    Board<String> B = BF.makeBoard(0, playerlist);
    Executor<String> E = new ServerExecutor<String>();
    Board<String> B1 = E.execute(B);
  }

}
