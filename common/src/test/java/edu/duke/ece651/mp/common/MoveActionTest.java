package edu.duke.ece651.mp.common;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class MoveActionTest {
    @Test
    public void test_move(){
        //Executor <String> executor = new ServerExecutor<>();
        ActionRecordHandler<String> handler = new ActionRecordHandler<>();
        Unit u = new Unit();
        u.setUnitsByLevel(0, 5);
        Player p = new Player(1, "yellow", 10);
        Territory<String> from = new V1Territory("Oz");
        Territory<String> to = new V1Territory("Des");
        MoveAction<String> move = new MoveAction<>(p, from, to, u);
        move.setFoodResCost(10);
        move.accept(handler);
        assertEquals(90, move.getPlayer().getFoodRes());
        assertEquals(100, move.getPlayer().getTechRes());
        assertEquals(-5, move.getFrom().getUnitsByLevel(0));
        assertEquals(0, move.getFrom().getUnitsByLevel(1));
        assertEquals(5, move.getTo().getUnitsByLevel(0));
    }

}
