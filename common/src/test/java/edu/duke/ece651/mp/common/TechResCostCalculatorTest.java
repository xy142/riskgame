package edu.duke.ece651.mp.common;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

public class TechResCostCalculatorTest {
    private Board<String> makeV1Boardby2(){
        V1BoardFactory fac = new V1BoardFactory(2);
        ArrayList<Player> playerList = new ArrayList<>();
        playerList.add(new Player(0,"blue",10));
        playerList.add(new Player(1,"yellow",10));
        Board<String> b = fac.makeBoard(0, playerList); 
        return b;
    }

    @Test
    public void test_TechCostCalcuator(){
        TechResCostCalculator<String> cal = new TechResCostCalculator<>();
        //Player player = new Player(0, "green", 10);
        Board<String> board = makeV1Boardby2();
        UpdateLevelAction<String> levelup = new UpdateLevelAction<>(board.getPlayerById(0));
        ActionHandler<String> handler = new ActionRecordHandler<>();
        levelup.setTechResCost(cal.computeTechResCost(levelup, board));
        levelup.accept(handler);
        assertEquals(50, board.getPlayerById(0).getTechRes());
        assertEquals(100, board.getPlayerById(0).getFoodRes());
        Unit u = new Unit();
        u.setUnitsByLevel(0, 5);
        u.setUnitsByLevel(1, 5);
        u.setUnitsByLevel(5, 5);
        UpdateUnitAction<String> unitUp = new UpdateUnitAction<>(board.getPlayerById(1), 
        board.getTerritoryByName("Mordor"), u);
        unitUp.setTechResCost(cal.computeTechResCost(unitUp, board));
        unitUp.accept(handler);
        assertEquals(100 - 5*3 - 5*8 - 5*50,board.getPlayerById(1).getTechRes());
        assertEquals(-5, board.getTerritoryByName("Mordor").getUnitsByLevel(0));
        assertEquals(0, board.getTerritoryByName("Mordor").getUnitsByLevel(1));
        assertEquals(5, board.getTerritoryByName("Mordor").getUnitsByLevel(2));
        assertEquals(5, board.getTerritoryByName("Mordor").getUnitsByLevel(6));
        assertEquals(-5, board.getTerritoryByName("Mordor").getUnitsByLevel(5));
    }
    
}
