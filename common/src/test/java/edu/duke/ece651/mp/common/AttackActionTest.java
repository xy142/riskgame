package edu.duke.ece651.mp.common;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

public class AttackActionTest {
    private Board<String> makeV1Boardby2(){
        V1BoardFactory fac = new V1BoardFactory(2);
        ArrayList<Player> playerList = new ArrayList<>();
        playerList.add(new Player(0,"blue",10));
        playerList.add(new Player(1,"yellow",10));
        Board<String> b = fac.makeBoard(0, playerList); 
        return b;
    }

    @Test
    public void test_attackAction(){
        Board<String> board = makeV1Boardby2();
        Unit units = new Unit();
        units.setUnitsByLevel(0, 2);
        units.setUnitsByLevel(1, 2);
        units.setUnitsByLevel(2, 2);
        units.setUnitsByLevel(6, 2);
        AttackAction<String> action = new AttackAction<>(board.getPlayerById(0), 
        board.getTerritoryByName("Oz"), board.getTerritoryByName("Mordor"), units);
        ActionHandler<String> handler = new ActionRecordHandler<>();
        action.accept(handler);
        Unit expected = new Unit();
        expected.setUnitsByLevel(0, -2);
        expected.setUnitsByLevel(1, -2);
        expected.setUnitsByLevel(2, -2);
        expected.setUnitsByLevel(6, -2);
        for(int i =0 ; i< units.getHighestLevel();i++){
        assertEquals(expected.getUnitsByLevel(i), board.getTerritoryByName("Oz").getUnitsByLevel(i));
        }

    }
    
}
