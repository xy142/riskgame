package edu.duke.ece651.mp.common;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class PlayerTest {
  @Test
  public void test_() {
    Player p1 = new Player(1, "Green", 10);
    Player p2 = new Player(1, "Green", 10);
    Player p3 = new Player(2, "Black", 10);
    V1Territory t1 = new V1Territory("Marcus",1, 10);
    assertEquals(10, p1.getTotalUnits());
    p1.lostOneUnit();
    p1.hashCode();
    assertEquals(9, p1.getTotalUnits());
    p1.gainOneUnit();
    
    assertEquals(10, p1.getTotalUnits());

    assertEquals("Green", p1.getPlayerColor());
    
    p1.setUnitsToPlace(20);
    assertEquals(20, p1.getUnitsToPlace());
    
    assertEquals(true, p1.equals(p2));
    assertEquals(false, p1.equals(p3));
    assertEquals(false, p1.equals(t1));
    assertEquals(true, p1.equals(p1));
    assertEquals(2, p3.getId());
    p1.commits();
    assertEquals(true, p1.hasCommitted());
    p1.resetCommitStatus();
    assertEquals(false, p1.hasCommitted());
    assertEquals(false, p1.loses());
    assertEquals(true, p1.wins());
    p1.setLoseToTrue();
    assertEquals(true, p1.loses());
   
   
   
   

  }

}
