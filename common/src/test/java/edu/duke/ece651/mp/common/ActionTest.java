package edu.duke.ece651.mp.common;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class ActionTest {
  @Test
  public void test_() {
    Player p1 = new Player(1, "Marcus", 10);
    Player p2 = new Player(2, "MAC", 10);
    V1Territory t1 = new V1Territory("Oz", 1, 10);
     V1Territory t2 = new V1Territory("Mordor", 2, 10);
     Unit u = new Unit();
     u.setUnitsByLevel(0, 5);
     Action A1 = new AttackAction<>(p1, t1, t2, u);
     assertEquals(t1, A1.getFrom());
     assertEquals(t2, A1.getTo());
     assertEquals(p1, A1.getPlayer());
     assertEquals(0, A1.getType());
     assertEquals(5, A1.getUnitsByLevel(0));
     assertEquals(p1, A1.getPlayer());
     A1.setPlayer(p2);
     assertEquals(p2, A1.getPlayer());
  
     A1.setFrom(t2);
     A1.setTo(t1);
     assertEquals(t2, A1.getFrom());
     assertEquals(t1, A1.getTo());
  

  }

}
