//package edu.duke.ece651.mp.common;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertThrows;
//
//import java.io.IOException;
//import java.io.ObjectInputStream;
//import java.io.ObjectOutputStream;
//import java.net.ServerSocket;
//import java.net.Socket;
//import java.net.UnknownHostException;
//import java.util.ArrayList;
//
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.Timeout;
//
//public class ConnectionHandlerTest {
//  @Test
//  @Timeout(2500)
//  public void test_()throws IOException, UnknownHostException, InterruptedException, ClassNotFoundException {
//    Thread th = new Thread(() ->{
//        try{
//        ServerSocket ss = new ServerSocket(12345);
//        Socket sock = ss.accept();
//        ObjectOutputStream out = new ObjectOutputStream(sock.getOutputStream());
//        ObjectInputStream in = new ObjectInputStream(sock.getInputStream());
//        Player p1 = new Player(1, "Marcus", 10);
//        Player p2 = new Player(2, "MAC", 10);
//        V1Territory t1 = new V1Territory("Oz", 1, 10);
//        V1Territory t2 = new V1Territory("Mordor", 2, 10);
//        Action A1 = new AttackAction<>(p1, t1, t2, 5);
//        ArrayList<Player> playerlist = new ArrayList<>();
//        playerlist.add(new Player(0, "GREEN", 10));
//        playerlist.add(new Player(1, "BLACK", 10));
//        playerlist.add(new Player(2, "RED", 10));
//        playerlist.add(new Player(3, "YELLOW", 10));
//
//        V1BoardFactory BF = new V1BoardFactory(4);
//        Board<String> B = BF.makeBoard(0, playerlist);
//        Response<String> res = new Response<>(p1, B, null, 0);
//
//        try{
//          in.readObject();
//          in.readObject();
//          out.writeObject(A1);
//          out.writeObject(res);
//        }catch(ClassNotFoundException e){
//
//        }
//        }catch(IOException  e){
//          System.err.println(e.getMessage());
//        }
//    });
//
//    th.start();
//    Thread.sleep(1000);
//    Socket s = new Socket("localhost", 12345);
//
//     ConnectionHandler<String> ch = new ConnectionHandler<String>(s, "client");
//     assertThrows(IllegalArgumentException.class, () ->new ConnectionHandler<String>(s, "clien"));
//     Player p1 = new Player(1, "Marcus", 10);
//     Player p2 = new Player(2, "MAC", 10);
//     V1Territory t1 = new V1Territory("Oz", 1, 10);
//     V1Territory t2 = new V1Territory("Mordor", 2, 10);
//     Action A1 = new AttackAction<>(p1, t1, t2, 5);
//     ch.sendActionRequest(A1);
//     ArrayList<Player> playerlist = new ArrayList<>();
//     playerlist.add(new Player(0, "GREEN", 10));
//     playerlist.add(new Player(1, "BLACK", 10));
//     playerlist.add(new Player(2, "RED", 10));
//     playerlist.add(new Player(3, "YELLOW", 10));
//
//     Player p3 = new Player(3, "YELLOW", 10);
//     V1BoardFactory BF = new V1BoardFactory(4);
//     Board<String> B = BF.makeBoard(0, playerlist);
//     Response<String> res = new Response<>(p1, B, null, 0);
//     assertEquals(false, ch.ifDisc());
//     ch.sendResponse(res);
//     ch.sendResponse(p1, B, null, 0);
//     ch.recvActionRequest();
//     ch.recvResponse();
//     /*assertEquals(A1.getClass(), ch.recvActionRequest());
//     assertEquals(res.getClass(), ch.recvResponse());
//     */
//     ch.disconnect();
//     assertEquals(true, ch.ifDisc());
//     ch.sendResponse(res);
//     ch.sendResponse(p1, B, null, 0);
//
//     s.close();
//
//
//  }
//
//}
