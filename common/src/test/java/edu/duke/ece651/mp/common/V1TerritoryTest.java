package edu.duke.ece651.mp.common;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

public class V1TerritoryTest {
  @Test
  public void test_terriConstruction() {
    Territory<String> t = new V1Territory("Oz", 1, 10);
    assertEquals("Oz", t.getName());
    assertEquals(1, t.getOwnerId());
    assertEquals(10, t.getUnitsByLevel(0));
  }
      @Test
      public void test_euqal(){
        Player p1 = new Player(1, "Marcus", 10);
        ArrayList<Territory<String> > terris = new ArrayList<>();
        Territory<String> t1 = new V1Territory("Oz", 1, 10);
        terris.add(t1);
        assertTrue(terris.contains(t1));
        Territory<String> t2 = new V1Territory("Oz", 2, 11);
        Territory<String> t3 = new V1Territory("OOO", 1, 10);
        assertTrue(t1.equals(t2));
        assertFalse(t1.equals(t3));
        assertFalse(t1.equals(p1));
        assertEquals(t1.getUnitsByLevel(0), 10);
        t1.setUnitsByLevel(1,30);
        assertEquals(t1.getUnitsByLevel(1), 30);
       
        t1.hashCode();
      }

}
