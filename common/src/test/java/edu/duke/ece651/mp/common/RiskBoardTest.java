package edu.duke.ece651.mp.common;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.jupiter.api.Test;

public class RiskBoardTest {


    private Board<String> makeV1Boardby2(){
        V1BoardFactory fac = new V1BoardFactory(2);
        ArrayList<Player> playerList = new ArrayList<>();
        playerList.add(new Player(0,"blue",0));
        playerList.add(new Player(1,"yellow",0));
        Board<String> b = fac.makeBoard(0, playerList); 
        return b;
    }

    @Test
    public void test_boardConstrctiontask1_2() {
        Territory<String> t = new V1Territory("Oz", 1, 10);
        HashMap<String, Territory<String>> terris = new HashMap<String, Territory<String>>();
        terris.put("Oz", t);
        Board<String> b = new RiskBoard<>(terris);
        assertEquals(t, b.getTerritoryByName("Oz"));
    }
    
    @Test
    public void test_isConnected() {
        Board<String> b = makeV1Boardby2();   
       // System.out.println(b.getTerritoryByName("Narnia"));
        assertTrue(b.isConnected("Oz", "Midkemia"));
        assertFalse(b.isConnected("Oz", "Hogwarts"));
        }
    @Test
    public void test_ifTerritoryExits() {
        Board<String> b = makeV1Boardby2();
        assertFalse(b.ifTerritoryExit("O"));
        assertTrue(b.ifTerritoryExit("Oz"));
    }
    @Test
    public void test_getTerritoryByPlayerId(){
        Board<String> b = makeV1Boardby2();
        ArrayList<String> expected = new ArrayList<>();
        ArrayList<String> actual = new ArrayList<>();
        for( var t: b.getTerritoryByPlayerId(0)){
            actual.add(t.getName());
        }
        expected.add("Oz");
        expected.add("Midkemia");
        expected.add("Narnia");
        expected.add("Gondor");
        assertEquals(expected, actual);
    }

    @Test
    public void test_updatePlayerTerris(){
        Board<String> b = makeV1Boardby2();
        b.getTerritoryByName("Oz").setOwnerId(1);
        b.updatePlayerTerris("Oz", 0);
        assertTrue(b.getTerritoryByPlayerId(1).contains(b.getTerritoryByName("Oz")));
        assertFalse(b.getTerritoryByPlayerId(0).contains(b.getTerritoryByName("Oz")));

    }

}
