package edu.duke.ece651.mp.common;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

public class ResponseTest {
  @Test
  public void test_() {
    ArrayList<Player> playerlist = new ArrayList<>();
    playerlist.add(new Player(0, "GREEN", 10));
    playerlist.add(new Player(1, "BLACK", 10));
    playerlist.add(new Player(2, "RED", 10));
    playerlist.add(new Player(3, "YELLOW", 10));
    Player p1 = new Player(0, "PINK", 10);
    V1BoardFactory BF = new V1BoardFactory(4);
    Board<String> B = BF.makeBoard(0, playerlist);
    Response<String> R = new Response(p1, B, "Error Message", 0);
    assertEquals(p1, R.getPlayer());
    assertEquals(B, R.getBoard());
    assertEquals("Error Message", R.getErrorMsg());
    assertEquals(0, R.getGamePhase());
  }

}
