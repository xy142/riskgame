package edu.duke.ece651.mp.common;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

public class FoodResCostCalculatorTest {
    private Board<String> makeV1Boardby2(){
        V1BoardFactory fac = new V1BoardFactory(2);
        ArrayList<Player> playerList = new ArrayList<>();
        playerList.add(new Player(0,"blue",10));
        playerList.add(new Player(1,"yellow",10));
        Board<String> b = fac.makeBoard(0, playerList); 
        return b;
    }

    @Test
    public void test_shortestRoute(){
        FoodResCostCalculator<String> cal = new FoodResCostCalculator<>();
        Board<String> board = makeV1Boardby2();
        Unit units = new Unit();
        units.setUnitsByLevel(0, 5);
        MoveAction<String> move = new MoveAction<>(board.getPlayerById(0),board.getTerritoryByName("Gondor"),
         board.getTerritoryByName("Narnia"), units);
        assertEquals(8*5, cal.computeFoodResCost(move, board));
        AttackAction<String> attack = new AttackAction<>(board.getPlayerById(0), board.getTerritoryByName("Gondor"),
        board.getTerritoryByName("Mordor"), units);
        assertEquals(5, cal.computeFoodResCost(attack, board));
    }
    
}
