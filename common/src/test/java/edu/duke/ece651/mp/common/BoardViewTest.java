package edu.duke.ece651.mp.common;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

public class BoardViewTest {
  @Test
  public void test_() {
     //V1Territory t1 = new V1Territory("Marcus", 1, 10);
    ArrayList<Player> playerlist = new ArrayList<>();
    playerlist.add(new Player(0, "GREEN", 10));
    playerlist.add(new Player(1, "BLACK", 10));
    playerlist.add(new Player(2, "RED", 10));
    playerlist.add(new Player(3, "YELLOW", 10));
    
    V1BoardFactory BF = new V1BoardFactory(4);
    Board<String> B = BF.makeBoard(0, playerlist);
    BoardView<String> BV = new BoardView<>(B);
    String ans = BV.getDisplay();
    System.out.println(B.getTerritoryByName("Midkemia").getUnitsByLevel(0));
    //TODO: Weird 10?
    assertEquals("GREEN player:\n" +
    "--------------------------------\n"+
    "[0,0,0,0,0,0,0] units in Narnia (next to: Elantris, Midkemia,)\n"+
    "[0,0,0,0,0,0,0] units in Midkemia (next to: Narnia, Elantris, Oz, Scadrial,)\n\n\n"+


   "BLACK player:\n"+
   "--------------------------------\n"+
   "[0,0,0,0,0,0,0] units in Gondor (next to: Oz, Mordor,)\n"+
   "[0,0,0,0,0,0,0] units in Oz (next to: Midkemia, Scadrial, Mordor, Gondor,)\n\n\n"+


    "RED player:\n"+
    "--------------------------------\n"+
    "[0,0,0,0,0,0,0] units in Mordor (next to: Oz, Gondor, Hogwarts, Scadrial,)\n"+
    "[0,0,0,0,0,0,0] units in Hogwarts (next to: Mordor, Roshar, Scadrial,)\n\n\n"+


    "YELLOW player:\n"+
    "--------------------------------\n"+
    "[0,0,0,0,0,0,0] units in Elantris (next to: Narnia, Midkemia, Scadrial, Roshar,)\n"+
    "[0,0,0,0,0,0,0] units in Scadrial (next to: Midkemia, Oz, Mordor, Hogwarts, Roshar, Elantris,)\n"+
    "[0,0,0,0,0,0,0] units in Roshar (next to: Scadrial, Hogwarts, Elantris,)\n\n\n", ans);
    int player = 1;
    ans = BV.OneDisplay(player);
    assertEquals("BLACK player:\n" +
    "--------------------------------\n" +
    "[0,0,0,0,0,0,0] units in Gondor (next to: Oz, Mordor,)\n" +
    "[0,0,0,0,0,0,0] units in Oz (next to: Midkemia, Scadrial, Mordor, Gondor,)\n\n\n", ans);

    assertEquals("This is your Border:\n", BV.showBorder());
    assertEquals("This is your Territory:\n", BV.showTerritory());
    assertEquals(true, B.isAdjacent(B.getTerritoryByName("Gondor").getName(), B.getTerritoryByName("Oz").getName()));
assertEquals(false, B.isAdjacent(B.getTerritoryByName("Roshar").getName(), B.getTerritoryByName("Oz").getName()));
   assertEquals(false, B.ifTerritoryExit("Marcus"));
   assertEquals(true, B.ifTerritoryExit("Oz"));
   assertEquals(null, B.getPlayerById(4));

  }

}
