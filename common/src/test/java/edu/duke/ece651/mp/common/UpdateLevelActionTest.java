package edu.duke.ece651.mp.common;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

public class UpdateLevelActionTest{
    private Board<String> makeV1Boardby2(){
        V1BoardFactory fac = new V1BoardFactory(2);
        ArrayList<Player> playerList = new ArrayList<>();
        playerList.add(new Player(0,"blue",0));
        playerList.add(new Player(1,"yellow",0));
        Board<String> b = fac.makeBoard(0, playerList); 
        return b;
    }

    @Test
    public void tets_updataLeve(){
        Board<String> board = makeV1Boardby2();
        Action<String> action = new UpdateLevelAction<>(board.getPlayerById(0));
        Executor<String> executor = new ServerExecutor<>();
        executor.tryRecordActionServer(action, board);
        executor.execute(board);
        assertEquals(2, board.getPlayerById(0).getTechLevel());
        assertEquals(1, board.getPlayerById(1).getTechLevel());
        executor.reset();
        Action<String> action1 = new UpdateLevelAction<>(board.getPlayerById(1));
        executor.tryRecordActionClient(action1, board);
        assertEquals(50, board.getPlayerById(1).getTechRes());
        executor.tryRecordActionServer(action1, board);
        executor.execute(board);
        assertEquals(2, board.getPlayerById(1).getTechLevel());
        assertEquals( "You don't have enough resources for this update.(" 
        + 75 +" needed but you only have "+ 50  ,executor.tryRecordActionServer(action, board));
        

    }
}
