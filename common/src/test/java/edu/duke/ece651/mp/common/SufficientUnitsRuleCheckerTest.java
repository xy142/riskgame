package edu.duke.ece651.mp.common;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

public class SufficientUnitsRuleCheckerTest {
  @Test
  public void test_() {
     ArrayList<Player> playerlist = new ArrayList<>();
    playerlist.add(new Player(0, "GREEN", 10));
    playerlist.add(new Player(1, "BLACK", 10));
    playerlist.add(new Player(2, "RED", 10));
    playerlist.add(new Player(3, "YELLOW", 10));
    
    V1BoardFactory BF = new V1BoardFactory(4);
    Board<String> B = BF.makeBoard(0, playerlist);
    Unit u = new Unit();
    u.setUnitsByLevel(0, 5);
    RuleChecker<String> check = new SufficientUnitsRuleChecker<String>(null);
    Action<String> A1 = new PlaceAction<>(playerlist.get(0),B.getTerritoryByName("Narnia"),B.getTerritoryByName("Midkemia"), u);
    assertEquals(null, check.checkMyRule(A1, B));
    u.setUnitsByLevel(0, 15);
     Action<String> A2 = new PlaceAction<>(playerlist.get(1), B.getTerritoryByName("Gondor"),B.getTerritoryByName("Narnia"), u);
    assertEquals("You only have 10 units to place.", check.checkMyRule(A2, B));    
    u.setUnitsByLevel(0, 5);
     Action<String> A3 = new AttackAction<>(playerlist.get(2), B.getTerritoryByName("Gondor"),B.getTerritoryByName("Midkemia"), u);
    assertEquals("You only have 0 units in Gondor.", check.checkMyRule(A3, B));    
    u.setUnitsByLevel(0, 0);
    A3 = new AttackAction<>(playerlist.get(2), B.getTerritoryByName("Gondor"),B.getTerritoryByName("Midkemia"), u);
    assertEquals(null, check.checkMyRule(A3, B));    
    A3 = new CommitAction<>(playerlist.get(2), B.getTerritoryByName("Gondor"),B.getTerritoryByName("Midkemia"), u);
    assertEquals(null, check.checkMyRule(A3, B));

  }

}
