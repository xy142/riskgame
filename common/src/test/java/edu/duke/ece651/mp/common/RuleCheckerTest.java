package edu.duke.ece651.mp.common;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

public class RuleCheckerTest {
  @Test
  public void test_() {

    ArrayList<Player> playerlist = new ArrayList<>();
    playerlist.add(new Player(0, "GREEN", 10));
    playerlist.add(new Player(1, "BLACK", 10));
    playerlist.add(new Player(2, "RED", 10));
    playerlist.add(new Player(3, "YELLOW", 10));
    
    V1BoardFactory BF = new V1BoardFactory(4);
    Board<String> B = BF.makeBoard(0, playerlist);

    RuleChecker<String> check = new AdjacentRuleChecker<String>(null);
    RuleChecker<String> check1 = new SufficientUnitsRuleChecker<String>(check);
    Unit u = new Unit();
    u.setUnitsByLevel(0, 0);
    Action<String> A1 = new AttackAction<>(playerlist.get(0), B.getTerritoryByName("Oz"),B.getTerritoryByName("Mordor"), u);
    assertEquals(null, check.checkMyRule(A1, B));    
    u.setUnitsByLevel(0, 5);
    Action<String> A2 = new AttackAction<>(playerlist.get(0), B.getTerritoryByName("Oz"),B.getTerritoryByName("Elantris"), u);
    assertEquals("Oz and Elantris are not adjacent.", check.checkMyRule(A2, B));    
     assertEquals("Oz and Elantris are not adjacent.",check.check(A2, B));
     //assertEquals(null, check1.checkMyRule(A1, B));
   
  }


  @Test
  public void test_1() {

    ArrayList<Player> playerlist = new ArrayList<>();
    playerlist.add(new Player(0, "GREEN", 10));
    playerlist.add(new Player(1, "BLACK", 10));
    playerlist.add(new Player(2, "RED", 10));
    playerlist.add(new Player(3, "YELLOW", 10));
    
    V1BoardFactory BF = new V1BoardFactory(4);
    Board<String> B = BF.makeBoard(0, playerlist);

    RuleChecker<String> check = new AdjacentRuleChecker<String>(null);
    RuleChecker<String> check1 = new SufficientUnitsRuleChecker<String>(check);
      Unit u = new Unit();
      u.setUnitsByLevel(0, 0);
    Action<String> A1 = new AttackAction<>(playerlist.get(0), B.getTerritoryByName("Oz"),B.getTerritoryByName("Mordor"), u);
       
    assertEquals(null, check1.check(A1, B));
   
  }

}
