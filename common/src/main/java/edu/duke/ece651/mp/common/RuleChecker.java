package edu.duke.ece651.mp.common;

public abstract class RuleChecker<T> {
    private final RuleChecker<T> next;

    public RuleChecker(RuleChecker<T> next) {
        this.next = next;
    }

    protected abstract String checkMyRule(Action<T> action, Board<T> board);

    public String check(Action<T> action, Board<T> board) {
        //if we fail our own rule: stop the placement is not legal
        String errorMsg = checkMyRule(action, board);
        if (errorMsg != null) {
            return errorMsg;
        }
        //other wise, ask the rest of the chain.
        if (next != null) {
            return next.check(action, board);
        }
        //if there are no more rules, then the placement is legal
        return null;
    }

}
