package edu.duke.ece651.mp.common;

import java.io.Serializable;

public class Player implements Serializable {
    private int id;
    private String playerColor;
    private String username;
    // the sum of 7 level units
    private int totalUnits;
    // at begining only 0 level units are given, so unitsToPlace are 0 level
    private int unitsToPlace;
    private boolean committed;
    private boolean lose;
    private boolean win;
    //for evol2
    // the initial techRes and foodRes are fixed for now
    private int techRes;
    private int foodRes;
    private int techLevel;


    public Player(int id, String playerColor, int totalUnits, int techLevel, int techRes, int foodRes) {
        this.id = id;
        this.playerColor = playerColor;
        this.totalUnits = this.unitsToPlace = totalUnits;
        this.win = true;
        this.lose = false;
        this.techLevel = techLevel;
        this.techRes = techRes;
        this.foodRes = foodRes;
    }
    public Player(int id, String playerColor, int totalUnits){
        //TODO: may need give it flexiblilty
        this(id, playerColor, totalUnits, 1, 100, 100);
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getId() { return id; }
    public void setLoseToTrue() { lose = true; }
    public boolean loses() { return lose; }
    public boolean wins() { return win; }
    public boolean hasCommitted() {
        return committed;
    }
    public void commits() { committed = true; }
    public void resetCommitStatus() { committed = false; }
    public String getPlayerColor() {
        return playerColor;
    }

    public int getTotalUnits() {
        return totalUnits;
    }

    public int getUnitsToPlace() {
        return unitsToPlace;
    }

    public void setUnitsToPlace(int units) {
        unitsToPlace = units;
    }






    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return playerColor.equals(player.playerColor);
    }

    @Override
    public int hashCode() {
        return playerColor.hashCode();
    }

    public void lostOneUnit() { totalUnits--; }
    public void gainOneUnit() { totalUnits++; }

    public int getTechRes() {
        return techRes;
    }

    public void setTechRes(int techRes) {
        this.techRes = techRes;
    }

    public int getFoodRes() {
        return foodRes;
    }

    public void setFoodRes(int foodRes) {
        this.foodRes = foodRes;
    }

    public int getTechLevel() {
        return techLevel;
    }

    // public void setTechLevel(int techLevel) {
    //     this.techLevel = techLevel;
    // }
    public void levelUp(){
        this.techLevel++;
    }
}
