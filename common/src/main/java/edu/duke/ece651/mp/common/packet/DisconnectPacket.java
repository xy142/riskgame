package edu.duke.ece651.mp.common.packet;

import edu.duke.ece651.mp.common.ConnectionHandler;

import java.io.IOException;

public class DisconnectPacket extends Packet {
    ConnectionHandler connectionHandler;
    public DisconnectPacket(String username, int playerId, ConnectionHandler connectionHandler) {
        super(username, playerId);
        this.connectionHandler = connectionHandler;
    }

    public ConnectionHandler getConnectionHandler() {
        return connectionHandler;
    }

    @Override
    public void accept(PacketHandler ph) {
        ph.visit(this);
    }
}
