package edu.duke.ece651.mp.common.packet;

import edu.duke.ece651.mp.common.Board;
import edu.duke.ece651.mp.common.Player;

import java.io.IOException;

public class GamePacket extends Packet {
    Player player;
    Board<?> board;

    public GamePacket(String username, int playerId, Player player, Board<?> board) {
        super(username, playerId);
        this.player = player;
        this.board = board;
    }

    public Player getPlayer() {
        return player;
    }

    public <T> Board<T> getBoard() {
        return (Board<T>) board;
    }

    @Override
    public void accept(PacketHandler ph) {
        ph.visit(this);
    }
}
