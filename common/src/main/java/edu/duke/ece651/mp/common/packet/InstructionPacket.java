package edu.duke.ece651.mp.common.packet;

public abstract class InstructionPacket extends Packet {
    public InstructionPacket(String username, int playerId) {
        super(username, playerId);
    }
}
