package edu.duke.ece651.mp.common;


import java.util.List;

/**
 * This is the interface of AbstractBoardFactory to generate board
 */
public interface AbstractBoardFactory<T> {

    /**
     * @return return the board using the ith board model
     */
    Board<T> makeBoard( int i, List<Player> playerList);

}
