package edu.duke.ece651.mp.common;

public class ConnectedRuleChecker<T> extends RuleChecker<T> {
    public ConnectedRuleChecker(RuleChecker<T> next) {
        super(next);
    }

    @Override
    protected String checkMyRule(Action<T> action, Board<T> board) {
        if (board.isConnected(action.getFrom().getName(), action.getTo().getName())) return null;
        return action.getFrom().getName() + " and " + action.getTo().getName() + " are not connected.";
    }
}
