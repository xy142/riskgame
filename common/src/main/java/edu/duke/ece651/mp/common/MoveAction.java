package edu.duke.ece651.mp.common;

public class MoveAction<T> extends Action<T>{

    public MoveAction(Player player,Territory<T> from, Territory<T> to, Unit units) {
        super(player,1,from,to,units);
    }

    @Override
    public void accept(ActionHandler<T> actionhandler) {
            actionhandler.visit(this);
    }
    
}
