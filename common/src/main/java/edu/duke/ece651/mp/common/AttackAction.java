package edu.duke.ece651.mp.common;

public class AttackAction<T> extends Action<T> {

    public AttackAction(Player player,Territory<T> from, Territory<T> to, Unit units) {
        super(player,0,from,to,units);
    }


    @Override
    public void accept(ActionHandler<T> actionhandler) {
            actionhandler.visit(this);
    }
    
}
