package edu.duke.ece651.mp.common;


public class CommitAction<T> extends Action<T>{
    public CommitAction(Player player,Territory<T> from, Territory<T> to, Unit units) {
        //TODO :: change 3 here, after regression test
        super(player,3,from,to,units);
    }
    public CommitAction(Player player) {
        this(player,null,null,new Unit(7));
    }


    @Override
    public void accept(ActionHandler<T> actionhandler) {
            actionhandler.visit(this);
    }

}
