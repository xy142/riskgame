package edu.duke.ece651.mp.common;


public class SufficientFoodResRuleChecker<T> extends RuleChecker<T> {
    private final FoodResCostCalculator<T> calculator = new FoodResCostCalculator<>();
    public SufficientFoodResRuleChecker(RuleChecker<T> next) {
        super(next);
    }
    @Override
    protected String checkMyRule(Action<T> action, Board<T> board) {
        int cost = calculator.computeFoodResCost(action,board);
        int foodRes = action.getPlayer().getFoodRes();
        if (cost > foodRes) {
            return "You only have " + foodRes
                    + " units food resource, but the smallest cost for this move is " 
                    + cost +".";
        }
        else{
            return null;
        }
    }

}
