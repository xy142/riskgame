package edu.duke.ece651.mp.common;

import java.util.ArrayList;
import java.util.List;

// this utility class would be used by both server and client sides to compute
//the food resouces needed for this action
public class FoodResCostCalculator<T> {
    // TODO:Test it later
    private int backtracking(Territory<T> curr, Territory<T> dest, int size, Board<T> board,
            List<Territory<T>> visited) {
        if (curr.equals(dest)) {
            //minSize = Math.min(size, minSize);
            return size;
        }
        int minS = Integer.MAX_VALUE;
        for (var n : board.getTerritoryNeighbour(curr.getName())) {
            if (visited.contains(n) || n.getOwnerId()!= dest.getOwnerId()) {
                continue;
            }
            visited.add(n);
            minS = Math.min(minS, backtracking(n, dest, size + curr.getSize(), board, visited));
        }
        return minS;
    }

    // after check isConnected return ture, then we call this function
    // using backtracking to find the best path
    /**
     * return the shortest length(total sizes) to get to the dest
     * 
     * @param terri1
     * @param terri2
     * @param board
     * @return
     */
    private int shortestRoute(Territory<T> terri1, Territory<T> terri2, Board<T> board) {
       //int minSize = Integer.MAX_VALUE;
        List<Territory<T>> visited = new ArrayList<>();
        return backtracking(terri1, terri2, 0,  board, visited);
    }

    // there are two actions comsuming the food resource
    public int computeFoodResCost(Action<T> action, Board<T> board) {
        int cost = 0;
        if (action.getType() == Action.MOVE) {
            cost = action.getUnits().getTotalUnits()
                    * shortestRoute(action.getFrom(), action.getTo(), board);
        } else if (action.getType() == Action.ATTACK) {
            cost = action.getUnits().getTotalUnits();

        }
        return cost;
    }

}
