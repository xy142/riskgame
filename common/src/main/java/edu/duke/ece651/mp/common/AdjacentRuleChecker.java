package edu.duke.ece651.mp.common;

public class AdjacentRuleChecker<T> extends RuleChecker<T> {
    public AdjacentRuleChecker(RuleChecker<T> next) {
        super(next);
    }

    @Override
    protected String checkMyRule(Action<T> action, Board<T> board) {

        if (board.isAdjacent(action.getFrom().getName(), action.getTo().getName())) return null;
        return action.getFrom().getName() + " and " + action.getTo().getName() + " are not adjacent.";

    }
}
