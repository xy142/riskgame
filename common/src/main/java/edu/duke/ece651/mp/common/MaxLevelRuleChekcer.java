package edu.duke.ece651.mp.common;

public class MaxLevelRuleChekcer<T> extends RuleChecker<T> {

    public MaxLevelRuleChekcer(RuleChecker<T> next) {
        super(next);
    }

    @Override
    protected String checkMyRule(Action<T> action, Board<T> board) {
        if(action.getPlayer().getTechLevel()>=6){
            //since the max level is 6, return false
            return "You can't do level up as you are now the heighest level";
        }
        return null;
    }
    
}
