package edu.duke.ece651.mp.common;

public class UpdateUnitAction<T> extends Action<T> {

    public UpdateUnitAction(Player player,Territory<T> from,  Unit units){
        super(player, 5, from, null, units);
    }

    @Override
    public void accept(ActionHandler<T> actionhandler) {
        actionhandler.visit(this);        
    }
    
}
