package edu.duke.ece651.mp.common;

import java.io.Serializable;

public class Unit implements Serializable{
    int[] units;

    public Unit(int totoallevels){
        units = new int[totoallevels];
    }
    
    public Unit(){
        units = new int[7];
    }

    public Unit(Unit another){
        units = new int[another.getHighestLevel()];
        for (int i =0; i < units.length; i++){
            this.units[i] = another.getUnitsByLevel(i);
        }
    }

    public void setUnits(Unit another){
        for (int i =0; i < units.length; i++){
            this.units[i] = another.getUnitsByLevel(i);
        }
    }


    public void setUnitsByLevel(int level, int amount){
        units[level] = amount;
    }

    public int getUnitsByLevel(int level){
        return units[level];
    }

    public int getHighestLevel(){
        return units.length;
    }

    public int getTotalUnits(){
        int sum =0;
        for (int i : units){
            sum += i;
        }
        return sum;
    }


    public Unit addUnits(Unit another, boolean add){
        for(int i =0; i < this.units.length; i++){    
            if(add){   
                units[i] += another.getUnitsByLevel(i);
            }
            else{
                units[i] -= another.getUnitsByLevel(i);
            }
        }
        return this;
    }
    // return the highest level unit for now
    public int getHighestLevelLeft(){
        for(int i =units.length-1; i >=0;i-- ){
            if(units[i]!=0){
                return i;
            }
        }
        throw new IllegalArgumentException("There is no unit left");
    }

    public int getLowestLevelLeft(){
        for(int i = 0; i< units.length; i++){
            if(units[i]!=0){
                return i;
            }
        }
        throw new IllegalArgumentException("There is no unit left");
    }





}
