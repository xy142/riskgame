package edu.duke.ece651.mp.common.packet;

import edu.duke.ece651.mp.common.Board;

public class BoardPacket<T> extends Packet {
    private Board<T> board;
    public BoardPacket(String username, int playerId, Board<T> board) {
        super(username, playerId);
        this.board = board;
    }

    public Board<T> getBoard() {
        return board;
    }

    public void accept(PacketHandler ph) throws InterruptedException {
        ph.visit(this);
    }
}
