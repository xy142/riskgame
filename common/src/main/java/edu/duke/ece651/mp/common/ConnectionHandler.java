package edu.duke.ece651.mp.common;

import edu.duke.ece651.mp.common.packet.Packet;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * This class is shared by the client and the server. The client may only call
 * sendActionRequest() and recvResponse(), and the server may only call
 * sendResponse() and recvActionRequest().
 */
public class ConnectionHandler {
    private final Socket socket;
    private final ObjectInputStream in;
    private final ObjectOutputStream out;
    // use this field to check if the socket still alive
    private boolean isConnected = true;

    public ConnectionHandler(Socket socket, String side) throws IOException {
        this.socket = socket;
        //TODO this place is not dry
        if (side.equals("server")) {
            in = new ObjectInputStream(socket.getInputStream());
            out = new ObjectOutputStream(socket.getOutputStream());
        } else if (side.equals("client")) {
            out = new ObjectOutputStream(socket.getOutputStream());
            in = new ObjectInputStream(socket.getInputStream());
        } else {
            throw new IllegalArgumentException("unknown parameter " + side);
        }
    }
    /**
     * client side sends action to server
     * @param packet
     * @throws IOException
     */
    public void sendPacket(Packet packet) throws IOException {
        out.reset();
        out.writeObject(packet);
    }

    /**
     *  server side receives actions client side sends
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public Packet recvPacket() throws IOException, ClassNotFoundException {
        return (Packet) in.readObject();
    }


    /**
     * if the server receive expcetion from the receActionRequest()
     * this means the socket with that client is disconnected
     * we set the isConnected bit to false
     */
    public void disconnect(){
        isConnected = false;
    }   
    /**
     * @return if the connection is interrupted
     */
    public boolean ifDisc(){
        return !isConnected;
    }

}
