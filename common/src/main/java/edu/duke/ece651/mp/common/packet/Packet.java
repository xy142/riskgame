package edu.duke.ece651.mp.common.packet;


import java.io.IOException;
import java.io.Serializable;

public abstract class Packet implements Serializable {

  protected String username = "";
  protected int uuid = -1;
  protected int playerId = -1;
  
  public Packet(String username, int playerId){
    this.username = username;
    this.playerId = playerId;
  }

  public String getUsername() {
    return username;
  }
  public int getPlayerId(){
    return playerId;
  }

  public void setUuid(int uuid) {
    this.uuid = uuid;
  }

  public int getUuid() {
    return uuid;
  }

  public abstract void accept(PacketHandler ph) throws IOException, InterruptedException;


}
