package edu.duke.ece651.mp.common.packet;

import java.io.IOException;

public class MessagePacket extends Packet {
    String message;
    public MessagePacket(String username, int playerId, String message) {
        super(username, playerId);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public void accept(PacketHandler ph) throws IOException {
        ph.visit(this);
    }
}
