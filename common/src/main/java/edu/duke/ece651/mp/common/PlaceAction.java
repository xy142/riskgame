package edu.duke.ece651.mp.common;

public class PlaceAction<T> extends Action<T> {

    public PlaceAction(Player player, Territory<T> from, Territory<T> to, Unit units) {
        //TODO :: change 2 here, after regression test
        super(player,2,from,to,units);
    }

    @Override
    public void accept(ActionHandler<T> actionhandler) {
            actionhandler.visit(this);
    }
    
}
