package edu.duke.ece651.mp.common.packet;

import java.io.IOException;

public class WaitPacket extends Packet {
    String errorMsg;
    public WaitPacket(String username, int playerId, String errorMsg) {
        super(username, playerId);
        this.errorMsg = errorMsg;
    }

    @Override
    public void accept(PacketHandler ph) throws IOException, InterruptedException {
        ph.visit(this);
    }
}
