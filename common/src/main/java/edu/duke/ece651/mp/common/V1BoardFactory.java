package edu.duke.ece651.mp.common;

import java.util.ArrayList;
import java.util.List;

public class V1BoardFactory implements AbstractBoardFactory<String> {

    public ArrayList<BoardModel<String>> boardModelList;

    // public V1BoardFactory(ArrayList<BoardModel<String>> boardModelList) {
    //     this.boardModelList = boardModelList;
    // }
    /**
     * Only one default board modle in the model list
     */
    public V1BoardFactory(int playerNum) {
        this.boardModelList = new ArrayList<>();
        boardModelList.add(new V1BoardModelDefault(playerNum));
       // boardModelList.add(new V1BoardModelLine(playerNum));
    }




    /**
     * @param i : the ith board model in the board model list
     * @return: an initialized board
     * For this step, The playerList still empty,
     * and all the terris are not assigned to each player
     *   
     * NOTE: We assumen all the necessary fields in the Player object has been filled 
     * e.g. id, initial units, color 
     */
    @Override
    public Board<String> makeBoard(int modelIdx,List<Player> playerList) {
        BoardModel<String> boardmodel = boardModelList.get(modelIdx);
        Board<String> board = new RiskBoard<String>(boardmodel.getterris(),
                boardmodel.getneighs(),
                boardmodel.getPlayerterris(),
                playerList);
        // now update the terris.owner
        board.initTerrisOwnerByGroup();
        // hardcoded the size for each territory
        //each player has the same sum of territories sum: 100 /10  =10;
        //beacuse 100 is the initial food res, 10 is the initial 0 level units, if size 10, then
        // just enough food res to move all the units
        board.initSizeForTerris();
        // set food production eqauls to size
        board.initFoodProduction();

        //set tech production eqauls to 5 * size
        board.initTechProduction();

        return board;
    }

}
