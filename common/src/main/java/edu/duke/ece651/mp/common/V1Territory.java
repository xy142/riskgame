package edu.duke.ece651.mp.common;

public class V1Territory implements Territory<String> {
    private final String name;
    private int ownerId;
    // we have 7 levels of units
    private Unit units;
    //evol2 
    // once decided, can't change it later
    private int foodProduction;
    private int techProduction;
    private int size;


    public V1Territory(String name, int ownerId, Unit units, int foodProduction, int techProduction, int size) {
        this.name = name;
        this.ownerId = ownerId;
        this.units = units;
        this.foodProduction = foodProduction;
        this.techProduction = techProduction;
        this.size = size;
    }

    /**
     * @param name
     * @param ownerId
     * @param units
     */
    public V1Territory(String name, int ownerId, int amount){
        this.name = name;
        this.ownerId = ownerId;
        this.units = new Unit(7);
        // this.units[0] = units;
        this.units.setUnitsByLevel(0, amount);
        this.size = 0;
        this.techProduction =0;
        this.foodProduction =0;
    }
    /**
     * this is the constructor that called by boardModel
     * @param name
     */
    public V1Territory(String name){
        this(name,-1,0); // the default value for userId and units 
    }


    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getOwnerId() {
        return ownerId;
    }

    /**
     * start from 0, 0-indexed 
     */
    @Override
    public int getUnitsByLevel(int level) {
        return units.getUnitsByLevel(level);
    }

    @Override
    public Unit getUnits(){
        return units;
    }

    @Override
    public void setOwnerId(int playerId) {
        this.ownerId = playerId;
    }


    @Override
    public void setUnitsByLevel(int level, int unitsToSet) {
        this.units.setUnitsByLevel(level, unitsToSet);        
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        V1Territory that = (V1Territory) o;
        return name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    public int getFoodProduction() {
        return foodProduction;
    }

    public void setFoodProduction(int foodProduction) {
        this.foodProduction = foodProduction;
    }

    public int getTechProduction() {
        return techProduction;
    }

    public void setTechProduction(int techProduction) {
        this.techProduction = techProduction;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public void setUnits(Unit another) {
        this.units.setUnits(another);
    }
}
