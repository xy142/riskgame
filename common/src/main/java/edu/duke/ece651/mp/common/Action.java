package edu.duke.ece651.mp.common;

import java.io.Serializable;

/**
 * This class represents a player's order.
 * It has 4 types defined in the enum, use format like Action.ACTION_TYPE.MOVE to compare types.
 */
public abstract class Action<T> implements Serializable {
    // action type
    public static final int ATTACK = 0;
    public static final int MOVE = 1;
    public static final int PLACE = 2;
    //for commit, all the other fileds are all ignored
    public static final int COMMIT = 3;
    //for update, all the other fileds are all ignored 
    public static final int UPDATELEVEL = 4;
    public static final int UPADTEUNIT = 5;


    private Territory<T> from;
    private Territory<T> to;
    private Unit units;
    private int type;
    private Player player;

    private int foodResCost;
    private int techResCost;

    //this interface is for visitor
    public abstract void accept(ActionHandler<T> actionhandler);

    public Action(Player player, int type, Territory<T> from, Territory<T> to, Unit units) {
        this.player = player;
        this.type = type;
        this.from = from;
        this.to = to;
        this.units = units;
        this.foodResCost =0;
        this.techResCost =0;
    }

    // empty action for players who lost game to return, commit action only
    public Action(Player player){
        this(player, 3, null, null,new Unit(7));
    }

  /**
   * This Method get Source Territory
   * @return Territory
   * @param 
   * @throws  
   */
    public Territory<T> getFrom() { return from; }
   /**
   * This Method get Destination Territory
   * @return Territory
   * @param 
   * @throws  
   */
    public Territory<T> getTo() { return to; }
    /**
   * This Method get the player that calls the Action
   * @return Player 
   * @param 
   * @throws  
   */
    public Player getPlayer() {
        return player;
    }
    /**
     * This Method get the playerId that calls the Action
     * @return Player
     * @param
     * @throws
     */
    public int getPlayerId() {
        return player.getId();
    }
   /**
   * This Method gets Units available for action
   * @return number of units
   * @param 
   * @throws  
   */
    public int getUnitsByLevel(int level) {
        return units.getUnitsByLevel(level);
    }

    public Unit getUnits(){
        return units;
    }

   /**
   * This Method gets the type of Action being called by player
   * @return Type of Action being called
   * @param 
   * @throws  
   */
    public int getType() {
        return type;
    }
    /**
   * This Method sets a Player to the Action being called
   * 
   * @param 
   * @throws  
   */
    public void setPlayer(Player player) {
        this.player = player;
    }
   /**
   * This Method set the from Territory to the Source
   * 
   * @param 
   * @throws  
   */
    public void setFrom(Territory<T> from) {
        this.from = from;
    }
   /**
   * This Method set the To Territory to the destination
   * 
   * @param 
   * @throws  
   */
    public void setTo(Territory<T> to) {
        this.to = to;
    }

public void setUnits(Unit units) {
    this.units = units;
}

public int getFoodResCost() {
    return foodResCost;
}

public void setFoodResCost(int foodResCost) {
    this.foodResCost = foodResCost;
}

public int getTechResCost() {
    return techResCost;
}

public void setTechResCost(int techResCost) {
    this.techResCost = techResCost;
}
}
