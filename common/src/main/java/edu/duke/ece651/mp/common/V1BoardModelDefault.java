package edu.duke.ece651.mp.common;

import java.util.ArrayList;
import java.util.HashMap;

public class V1BoardModelDefault extends BoardModel<String> {
    // the terris in the board
    private final static HashMap<String, Territory<String>> modelTerris = new HashMap<>() {
        {
            put("Oz", new V1Territory("Oz"));
            put("Narnia", new V1Territory("Narnia"));
            put("Midkemia", new V1Territory("Midkemia"));
            put("Gondor", new V1Territory("Gondor"));
            put("Elantris", new V1Territory("Elantris"));
            put("Scadrial", new V1Territory("Scadrial"));
            put("Mordor", new V1Territory("Mordor"));
            put("Roshar", new V1Territory("Roshar"));
            put("Hogwarts", new V1Territory("Hogwarts"));

        }
    };
    // the topology of the board
    private final static HashMap<String, ArrayList<String>> neighs = new HashMap<>() {
        {   put("Oz", new ArrayList<>());
            // neigh for oz
            get("Oz").add( "Midkemia");
             get("Oz").add( "Scadrial");
             get("Oz").add( "Mordor");
             get("Oz").add( "Gondor");
            // neigh for narnia
            put("Narnia", new ArrayList<>());
             get("Narnia").add( "Elantris");
             get("Narnia").add( "Midkemia");
            // neigh for midkemia
            put( "Midkemia", new ArrayList<>());
             get("Midkemia").add( "Narnia");
             get("Midkemia").add( "Elantris");
             get("Midkemia").add( "Oz");
             get("Midkemia").add( "Scadrial");
            // neigh for Gondor
            put( "Gondor", new ArrayList<>());
             get("Gondor").add( "Oz");
             get("Gondor").add( "Mordor");
            // neigh for Elantris
            put( "Elantris", new ArrayList<>());
             get("Elantris").add( "Narnia");
             get("Elantris").add( "Midkemia");
             get("Elantris").add( "Scadrial");
             get("Elantris").add( "Roshar");
            // neigh for Scadrial
            put( "Scadrial", new ArrayList<>());
             get("Scadrial").add( "Midkemia");
             get("Scadrial").add( "Oz");
             get("Scadrial").add( "Mordor");
             get("Scadrial").add( "Hogwarts");
             get("Scadrial").add( "Roshar");
             get("Scadrial").add( "Elantris");
            // neigh for Mordor
            put( "Mordor", new ArrayList<>());
             get("Mordor").add( "Oz");
             get("Mordor").add( "Gondor");
             get("Mordor").add( "Hogwarts");
             get("Mordor").add( "Scadrial");
            // neigh for Roshar
            put( "Roshar", new ArrayList<>());
             get("Roshar").add( "Scadrial");
             get("Roshar").add( "Hogwarts");
             get("Roshar").add( "Elantris");
            // neigh for Hogwarts
            put( "Hogwarts", new ArrayList<>());
             get("Hogwarts").add( "Mordor");
             get("Hogwarts").add( "Roshar");
             get("Hogwarts").add( "Scadrial");
        }
    };

    // group the terris
    static HashMap<Integer, ArrayList<String> > makePlayerTerris( int playerNum) {
        HashMap<Integer, ArrayList<String> > terrisByGroup = new HashMap<Integer, ArrayList<String> >();
        for( int i =0; i < playerNum; i++){
            terrisByGroup.put(i, new ArrayList<>());
        }
        if(playerNum == 2){
            terrisByGroup.get(0).add("Oz");
            terrisByGroup.get(0).add("Midkemia");
            terrisByGroup.get(0).add("Narnia");
            terrisByGroup.get(0).add("Gondor");
            terrisByGroup.get(1).add("Mordor");
            terrisByGroup.get(1).add("Elantris");
            terrisByGroup.get(1).add("Scadrial");
            terrisByGroup.get(1).add("Roshar");
            terrisByGroup.get(1).add("Hogwarts");
        }
        else if(playerNum ==3){
            terrisByGroup.get(0).add("Oz");
            terrisByGroup.get(0).add("Midkemia");
            terrisByGroup.get(0).add("Narnia");
            terrisByGroup.get(1).add("Gondor");
            terrisByGroup.get(1).add("Mordor");
            terrisByGroup.get(1).add("Hogwarts");
            terrisByGroup.get(2).add("Elantris");
            terrisByGroup.get(2).add("Scadrial");
            terrisByGroup.get(2).add("Roshar");
        }
        else if(playerNum ==4) {
            terrisByGroup.get(0).add("Midkemia");
            terrisByGroup.get(0).add("Narnia");
            terrisByGroup.get(1).add("Oz");
            terrisByGroup.get(1).add("Gondor");
            terrisByGroup.get(2).add("Mordor");
            terrisByGroup.get(2).add("Hogwarts");
            terrisByGroup.get(3).add("Elantris");
            terrisByGroup.get(3).add("Scadrial");
            terrisByGroup.get(3).add("Roshar");
            
        }
        else if(playerNum ==5) {
            terrisByGroup.get(0).add("Midkemia");
            terrisByGroup.get(0).add("Narnia");
            terrisByGroup.get(1).add("Oz");
            terrisByGroup.get(1).add("Gondor");
            terrisByGroup.get(2).add("Mordor");
            terrisByGroup.get(2).add("Hogwarts");
            terrisByGroup.get(3).add("Scadrial");
            terrisByGroup.get(3).add("Roshar");
            terrisByGroup.get(4).add("Elantris");
        }
        else{
            throw new IllegalArgumentException("The player number should be 2-5: " + playerNum);
        }

        return terrisByGroup;
    }








    // also the food production




    // tech producion



    // call the parent constructor
    public V1BoardModelDefault(int playerNum) {
        super(modelTerris,makePlayerTerris(playerNum),neighs);
    }

}
