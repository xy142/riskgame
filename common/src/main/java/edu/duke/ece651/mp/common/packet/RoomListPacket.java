package edu.duke.ece651.mp.common.packet;

import edu.duke.ece651.mp.common.RoomInfo;


import java.io.IOException;
import java.util.List;

public class RoomListPacket extends Packet {
    List<RoomInfo> roomInfoList;
    public RoomListPacket(String username, int playerId, List<RoomInfo> roomInfoList) {
        super(username, playerId);
        this.roomInfoList = roomInfoList;
    }

    public List<RoomInfo> getRoomInfoList() {
        return roomInfoList;
    }

    @Override
    public void accept(PacketHandler ph) throws IOException, InterruptedException {
        ph.visit(this);
    }
}
