package edu.duke.ece651.mp.common;

public class PlaceOwnerRuleChecker<T> extends RuleChecker<T> {
    public PlaceOwnerRuleChecker(RuleChecker<T> next) {
        super(next);
    }

    @Override
    protected String checkMyRule(Action<T> action, Board<T> board) {
        if (action.getTo().getOwnerId() !=action.getPlayer().getId())
            return "You don't own " + action.getTo().getName() + ".";
        return null;

    }
}
