package edu.duke.ece651.mp.common.packet;

import java.io.IOException;

public class SignUpResultPacket extends Packet {
    String errorMsg;
    public SignUpResultPacket(String username, int playerId, String errorMsg) {
        super(username, playerId);
        this.errorMsg = errorMsg;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    @Override
    public void accept(PacketHandler ph) throws IOException, InterruptedException {
        ph.visit(this);
    }
}
