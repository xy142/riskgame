package edu.duke.ece651.mp.common.packet;

import java.io.IOException;

public interface PacketHandler {
    public <T> void visit(BoardPacket<T> p) throws InterruptedException;
    public void visit(PlayerPacket p);
    public void visit(ActionPacket p) throws InterruptedException;
    public <T> void visit(CheckResultPacket p) throws IOException, InterruptedException;
    public void visit(PlacePacket p) throws IOException, InterruptedException;
    public void visit(MainPacket p) throws IOException;
    public void visit(WaitPacket waitPacket);
    public void visit(EndPacket p);
    public void visit(MessagePacket p);
    public void visit(CreateRoomPacket createRoomPacket) throws InterruptedException;
    public void visit(SignUpPacket signUpPacket) throws InterruptedException;
    public void visit(LogInPacket logInPacket) throws InterruptedException;
    public void visit(RoomListPacket roomListPacket) throws IOException;
    public void visit(GetRoomListPacket getRoomListPacket) throws InterruptedException;
    public void visit(EnterRoomPacket enterRoomPacket) throws InterruptedException;
    public void visit(GamePacket gamePacket);
    public void visit(DisconnectPacket disconnectPacket);
    public void visit(LogInSuccessPacket logInSuccessPacket);
    public void visit(SignUpResultPacket signUpFailurePacket) throws IOException;
    public void visit(LogInFailurePacket logInFailurePacket) throws IOException;
    public void visit(EnterRoomFailurePacket enterRoomFailurePacket);
    public void visit(LogOutPacket logOutPacket);
    public void visit(LeaveGamePacket leaveGamePacket) throws InterruptedException;
}
