package edu.duke.ece651.mp.common;

public class MoveOwnerRuleChecker<T> extends RuleChecker<T> {
    public MoveOwnerRuleChecker(RuleChecker<T> next) {
        super(next);
    }

    @Override
    protected String checkMyRule(Action<T> action, Board<T> board) {
      if (action.getFrom().getOwnerId()!=action.getPlayer().getId()){
            return "You don't own " + action.getFrom().getName() + ".";
      }
      if (action.getTo().getOwnerId()!=action.getPlayer().getId()){
            return "You don't own " + action.getTo().getName() + ".";
      }
        return null;
    }
}
