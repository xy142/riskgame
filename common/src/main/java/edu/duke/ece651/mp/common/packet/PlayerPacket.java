package edu.duke.ece651.mp.common.packet;

import edu.duke.ece651.mp.common.Player;

public class PlayerPacket extends Packet {
    private Player player;
    public PlayerPacket(String username, int playerId, Player player) {
        super(username, playerId);
        this.player = player;
    }
    public Player getPlayer() {
        return player;
    }
    @Override
    public void accept(PacketHandler ph) {
        ph.visit(this);
    }
}
