package edu.duke.ece651.mp.common.packet;

import edu.duke.ece651.mp.common.Action;

public class ActionPacket extends Packet {
    private Action<?> action;

    public <T> ActionPacket(String username, int playerId, Action<T> action) {
        super(username, playerId);
        this.action = action;
    }


    @Override
    public void accept(PacketHandler ph) throws InterruptedException {
        ph.visit(this);
    }

    public <T> Action<T> getAction() {
        return (Action<T>) action;
    }
}
