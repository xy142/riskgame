package edu.duke.ece651.mp.common;

import java.io.Serializable;
/**
 * The board is to extends serializable interface so that it can be sent 
 * between clients and server
 * Classes implement it if they want their instances to be Serialized or Deserialized.
 * Serialization is a mechanism of converting the state of an object into a byte stream. 
 * Serialization is done using ObjectOutputStream. Deserialization is the reverse process 
 * where the byte stream is used to recreate the actual Java object in memory. 
 * This mechanism is used to persist the object. Deserialization is done using ObjectInputStream.
 *  Thus it can be used to make an eligible for saving its state into a file. 
 */
import java.util.Collection;
public interface Board<T> extends Serializable {

    /**
     * 
     * @return the total number of players in the game
     */
     public int getPlayerNum();

    /**
     * This function is to initialize the territories by group since makeBoard() has already
     * created a PlayerTerris hashmap; refer to the hasmap and initialize the owner of each Terri
     * @param playerNum
     */
     public void initTerrisOwnerByGroup();


    /**
     * this function is to set the size for each territory
     */
     public void initSizeForTerris();

     public void initTechProduction();
     public void initFoodProduction();
    /**
     * This function will return the neighbour territories of the target terri
     * @param territory: the neighbours of which territoy we are looking at 
     * @return
     */
    public Collection<Territory<T> > getTerritoryNeighbour( String  territory);

    /**
     * This function will check if two territories are connceted, i.e. can move from 1 to2
     * This basically means there is a path from 1 to 2
     * NOTE: distinguish it from isAdjacent
     * @param terri1 
     * @param terri2
     * @return
     */
    public boolean isConnected(String terri1, String terri2);

    /**
     * 
     * @param terri1
     * @param terri2
     * @return
     */
    public boolean isAdjacent(String terri1, String terri2);

    /**
     * 
     * @param name
     * @return a territory obejct by the string name
     */
    public Territory<T> getTerritoryByName(String name);

    /**
     * check if a territory exits 
     * @param name
     * @return 
     */
    public boolean ifTerritoryExit(String name);

    /**
     * 
     * @return all the Territories on the board
     */
    public Collection<Territory<T> > getAllTerritory();

    /**
     * 
     * @param id
     * @return all the Terris a player owns
     */
    public Collection<Territory<T> > getTerritoryByPlayerId(int id);

    /**
     * return the player obecjt by its id
     * @param id
     * @return
     */
    public Player getPlayerById(int id);

    /**
     * this method is aimed to update the playerTerris after move/attack action changes 
     * the onwer of one territory 
     */
    public void updatePlayerTerris( String terri, int oldOwner);


}
