package edu.duke.ece651.mp.common;

import java.util.*;

import com.google.common.net.InetAddresses.TeredoInfo;

public class ServerExecutor<T> extends Executor<T> {
    // save the players who want to level up this turn, and do it after attack

    // TODO:test later
    private static final Map<Integer, Integer> bonusTable = new HashMap<>() {
        {
            put(0, 0);
            put(1, 1);
            put(2, 3);
            put(3, 5);
            put(4, 8);
            put(5, 11);
            put(6, 15);
        }
    };

    Random random = new Random();

    /**
     * server side compute actions result here
     */
    @Override
    public Board<T> execute(Board<T> board) {
        Collection<Territory<T>> territories = board.getAllTerritory();
        // converge the attakers who attacks the same territory
        // (dest,(player i, units to attack))
        HashMap<Territory<T>, HashMap<Player, Unit>> map = new HashMap<>();
        for (var action : actionRecord) {
            if (!map.containsKey(action.getTo())) {
                map.put(action.getTo(), new HashMap<>());
                map.get(action.getTo()).put(action.getPlayer(), new Unit(action.getUnits()));

            } else {
                if (!map.get(action.getTo()).containsKey(action.getPlayer())) {
                    // player does not exist
                    map.get(action.getTo()).put(action.getPlayer(), new Unit(action.getUnits()));
                } else {
                    // player exists
                    map.get(action.getTo()).put(action.getPlayer(),
                            map.get(action.getTo()).get(action.getPlayer()).addUnits(action.getUnits(), true));
                }
            }
        }

        for (var territory : map.keySet()) {
            // attackers is a hashmap, each entry is (player, units to attack )
            var attackers = map.get(territory);
            var defender = board.getPlayerById(territory.getOwnerId());
            var defenderUnits = territory.getUnits();
            // logic here : a terri owned by D, A,B,C want to attack this terri
            // A,B,C -> D; 1. A->D, if A win then 2. B->A, if A win then 3. C->A, if C win
            // 4. C is final owner
            for (var pair : attackers.entrySet()) {
                var attacker = pair.getKey();
                Unit attackerUnits = pair.getValue();
                boolean attackTurn = true;
                while (defenderUnits.getTotalUnits() != 0 && attackerUnits.getTotalUnits() != 0) {
                    oneUnitFight(attackerUnits, defenderUnits, attackTurn, attacker, defender);
                    attackTurn = !attackTurn;
                }
                // attacker win
                if (attackerUnits.getTotalUnits() != 0) {
                    defender = attacker;
                    defenderUnits = attackerUnits;
                }
            }
            // update the defender info
            territory.setUnits(defenderUnits);
            int oldOwner = territory.getOwnerId();
            territory.setOwnerId(defender.getId());
            // update the playerTerrris as well
            board.updatePlayerTerris(territory.getName(), oldOwner);
            // still in the for loop, to see next terri
        }
        // update the level for each player here if they do level up action
        for (Action<T> action : levelUpRecord) {
            action.getPlayer().levelUp();
        }
        return board;
    }

    private void oneUnitFight(Unit attackerUnits, Unit defenderUnits, boolean attackTurn, Player attacker,
            Player defender) {
        int bonusAck = 0;
        int bonusDef = 0;
        int attackUnitLevel = 0;
        int defendUnitLevel = 0;
        // attackTurn : the attacker highest unit attacks defender lowest unit
        // defnedTurn: turn over: the defender highest unit attacks the attacker lowest
        if (attackTurn) {
            attackUnitLevel = attackerUnits.getHighestLevelLeft();
            defendUnitLevel = defenderUnits.getLowestLevelLeft();
        } else {
            attackUnitLevel = attackerUnits.getLowestLevelLeft();
            defendUnitLevel = defenderUnits.getHighestLevelLeft();
        }
        bonusAck = bonusTable.get(attackUnitLevel);
        bonusDef = bonusTable.get(defendUnitLevel);
        // add bonus here fore v2
        int defenderRandomNumber = random.nextInt(20) + bonusDef;
        int attackerRandomNUmber = random.nextInt(20) + bonusAck;
        if (defenderRandomNumber > attackerRandomNUmber) {
            // player totalUnit-1
            attacker.lostOneUnit();
            // decrease corresponding level unit amount by 1
            // attackerUnits--;
            attackerUnits.setUnitsByLevel(attackUnitLevel,
                    attackerUnits.getUnitsByLevel(attackUnitLevel) - 1);
        } else if (attackerRandomNUmber > defenderRandomNumber) {
            defender.lostOneUnit();
            // defenderUnits--;
            defenderUnits.setUnitsByLevel(defendUnitLevel,
                    defenderUnits.getUnitsByLevel(defendUnitLevel) - 1);
        }

    }

    @Override
    public Board<T> addBaseUnitForAllPlayer(Board<T> board) {
        // add one units of each territory
        for (var terri : board.getAllTerritory()) {
            terri.setUnitsByLevel(0, terri.getUnitsByLevel(0) + 1);
            // update player total units
            board.getPlayerById(terri.getOwnerId()).gainOneUnit();
        }
        // add food resource to each player + add tech resource to each player
        for (int i = 0; i < board.getPlayerNum(); i++) {
            int allFoodProduction = 0;
            int allTechProduction = 0;
            for (var terri : board.getTerritoryByPlayerId(i)) {
                allFoodProduction += terri.getFoodProduction();
                allTechProduction += terri.getTechProduction();
            }
            board.getPlayerById(i).setFoodRes(board.getPlayerById(i).getFoodRes()
                    + allFoodProduction);
            board.getPlayerById(i).setTechRes(board.getPlayerById(i).getTechRes()
                    + allTechProduction);
        }
        return board;
    }

}
