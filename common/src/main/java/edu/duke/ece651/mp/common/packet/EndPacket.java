package edu.duke.ece651.mp.common.packet;

public class EndPacket extends InstructionPacket {
    String winnerInfo;
    public EndPacket(String username, int playerId, String winnerInfo) {
        super(username, playerId);
        this.winnerInfo = winnerInfo;
    }

    public String getWinnerInfo() {
        return winnerInfo;
    }

    @Override
    public void accept(PacketHandler ph) {
        ph.visit(this);
    }
}
