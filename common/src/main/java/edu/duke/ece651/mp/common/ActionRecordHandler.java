package edu.duke.ece651.mp.common;

public class ActionRecordHandler<T> implements ActionHandler<T> {


    private void setUnitsForTerri(Territory<T> terri, Action<T> action, boolean add){
        for (int i =0; i < action.getUnits().getHighestLevel(); i++){
            if(add){
                terri.setUnitsByLevel(i, terri.getUnitsByLevel(i) + action.getUnitsByLevel(i));
            }
            else{
                terri.setUnitsByLevel(i, terri.getUnitsByLevel(i) - action.getUnitsByLevel(i));

            }
        }
    }

    @Override
    public void visit(AttackAction<T> action) {
        //deduce the units of own territory
        // for (int i =0; i < action.getUnitsLen(); i++){
        //     action.getFrom().setUnitsByLevel(i, action.getFrom().getUnitsByLevel(i) - action.getUnitsByLevel(i));
        // }
        setUnitsForTerri(action.getFrom(), action, false);
        // player food resource need be deduced 
        int newFoodRes = action.getPlayer().getFoodRes() - action.getFoodResCost();
        action.getPlayer().setFoodRes(newFoodRes);

    }


    @Override
    public void visit(MoveAction<T> action) {
        // action.getFrom().setUnits(action.getFrom().getUnits() - action.getUnits());
        // action.getTo().setUnits(action.getTo().getUnits() + action.getUnits());
        setUnitsForTerri(action.getFrom(), action, false);
        setUnitsForTerri(action.getTo(), action, true);
        // player food resource need be deduced 
        int newFoodRes = action.getPlayer().getFoodRes() - action.getFoodResCost();
        action.getPlayer().setFoodRes(newFoodRes);

    }

    @Override
    public void visit(PlaceAction<T> action) {
        // action.getTo().setUnits(action.getTo().getUnits() + action.getUnits());
        // Note: at begining only 0 level units are given
         action.getPlayer().setUnitsToPlace(action.getPlayer().getUnitsToPlace() - action.getUnitsByLevel(0));
        setUnitsForTerri(action.getTo(), action, true);
        
    }

    @Override
    public void visit(CommitAction<T> action) {
        action.getPlayer().commits();
    }

    @Override
    public void visit(UpdateLevelAction<T> action) {
        //the levelUp will de done by executor at the end of the game
        //action.getPlayer().levelUp();
        // player tech resource need be deduced 
       action.getPlayer().setTechRes(action.getPlayer().getTechRes()-action.getTechResCost());
    }

    @Override
    public void visit(UpdateUnitAction<T> action) {
        // update the units in territory
        var terri = action.getFrom();
        //NOTE: our rule check will make sure there is no way to update the highestLevel unit
        // therefor,action.getUnits().getHighestLevel()-1
        for( int i =0; i < action.getUnits().getHighestLevel()-1; i++){
            int changeForPerLevel = action.getUnitsByLevel(i);
            //decrease the current level by the amounts of that level in action
            terri.getUnits().setUnitsByLevel(i, terri.getUnitsByLevel(i)-changeForPerLevel);
            //increase the next level by by the amounts of taht level in action
            terri.getUnits().setUnitsByLevel(i+1, terri.getUnitsByLevel(i+1)+ changeForPerLevel);
        }
        // player tech resource need be deduced 
        action.getPlayer().setTechRes(action.getPlayer().getTechRes()-action.getTechResCost());

    }
    
}
