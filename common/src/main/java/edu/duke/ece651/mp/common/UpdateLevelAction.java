package edu.duke.ece651.mp.common;

public class UpdateLevelAction<T> extends Action<T> {

    public UpdateLevelAction(Player player) {
        super(player,4,null,null,new Unit());
    }

    @Override
    public void accept(ActionHandler<T> actionhandler) {
        actionhandler.visit(this);        
    }
    
}
