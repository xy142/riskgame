package edu.duke.ece651.mp.common;

public class SufficientTechResRuleChecker<T> extends RuleChecker<T> {
    private final TechResCostCalculator<T> techCal = new TechResCostCalculator<>();
    public SufficientTechResRuleChecker(RuleChecker<T> next) {
        super(next);
    }
    @Override
    protected String checkMyRule(Action<T> action, Board<T> board) {
        String msg = null;
        int cost = techCal.computeTechResCost(action, board);
        int resHave = action.getPlayer().getTechRes();
        // if (action.getType() == Action.UPDATELEVEL) {
        //     // get the level and tech resource it need
        //     System.out.println(action.getPlayer().getTechLevel());
        //     cost = playerLevelUpTable.get(action.getPlayer().getTechLevel());
        // }
        // else if (action.getType() == Action.UPADTEUNIT){
        //     for( int i =0; i < action.getUnits().getHighestLevel();i++){
        //         cost += unitsLevelUpTable.get(i) * action.getUnitsByLevel(i);
        //     }
        // }
       if(resHave< cost){
            msg = "You don't have enough resources for this update.(" 
            + cost +" needed but you only have "+ resHave;
         }
        return msg;
    }

}
