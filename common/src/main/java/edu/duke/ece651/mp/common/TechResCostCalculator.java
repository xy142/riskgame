package edu.duke.ece651.mp.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TechResCostCalculator<T> {
    private static final HashMap<Integer, Integer> playerLevelUpTable = new HashMap<>() {
        {
            put(1, 50); // 1->2
            put(2, 75); // 2->3
            put(3, 125); // 3->4
            put(4, 200); // 4->5
            put(5, 300); // 5->6
        }
    };

    private static final List<Integer> unitsLevelUpTable = new ArrayList<>() {
        {
            add(3); // 0->1 costs 3 for each unit
            add(8); // 1->2 costs 8 for each unit
            add(19); // 2->3 costs 19 for each unit
            add(25); // 3->4 costs 25 for each
            add(35); // 4->5 35
            add(50); // 5->6 50
        }

    };

    public int computeTechResCost(Action<T> action, Board<T> board) {
        int cost = 0;
        if (action.getType() == Action.UPDATELEVEL) {
            // get the level and tech resource it need
            cost = playerLevelUpTable.get(action.getPlayer().getTechLevel());
        } 
        else if (action.getType() == Action.UPADTEUNIT) {
            for (int i = 0; i < action.getUnits().getHighestLevel()-1; i++) {
                cost += unitsLevelUpTable.get(i) * action.getUnitsByLevel(i);
            }
        }
        return cost;

    }
}
