package edu.duke.ece651.mp.common;

public class PlaceFinishRuleChecker<T> extends RuleChecker<T>{
//this will be checked for commit action
    public PlaceFinishRuleChecker(RuleChecker<T> next) {
        super(next);
    }

    @Override
    protected String checkMyRule(Action<T> action, Board<T> board) {
        if(action.getPlayer().getUnitsToPlace()!=0){
            return "Cannot commit until you place all the units";
        }
        return null;
    }
    
}
