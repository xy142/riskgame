package edu.duke.ece651.mp.common;

import java.io.Serializable;

public class Response<T> implements Serializable {
    private Player player;
    private Board<T> board;
    private String errorMsg;
    private int gamePhase;

    public Response(Player player, Board<T> board, String errorMsg, int gamePhase) {
        this.player = player;
        this.board = board;
        this.errorMsg = errorMsg;
        this.gamePhase = gamePhase;
    }

    public Player getPlayer() {
        return player;
    }

    public Board<T> getBoard() {
        return board;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public int getGamePhase() {
        return gamePhase;
    }
}
