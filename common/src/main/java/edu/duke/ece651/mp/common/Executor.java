package edu.duke.ece651.mp.common;

import java.util.ArrayList;
import java.util.HashMap;

public abstract class Executor<T> {
    private final FoodResCostCalculator<T> foodCal = new FoodResCostCalculator<>();
    private final TechResCostCalculator<T> tecCal = new TechResCostCalculator<>();

    protected HashMap<Integer , RuleChecker<T>> rulecheckers = new HashMap<>() {
        {
            put(Action.ATTACK, new AttackOwnerRuleChecker<>(new AdjacentRuleChecker<>(new SufficientUnitsRuleChecker<>(new SufficientFoodResRuleChecker<>(null)))));
            put(Action.MOVE, new MoveOwnerRuleChecker<>(new ConnectedRuleChecker<>(new SufficientUnitsRuleChecker<>(new SufficientFoodResRuleChecker<>(null)))));
            put(Action.PLACE, new PlaceOwnerRuleChecker<>(new SufficientUnitsRuleChecker<>(null)));
            put(Action.COMMIT, new PlaceFinishRuleChecker<>(null));
            put(Action.UPDATELEVEL, new MaxLevelRuleChekcer<>(new SufficientTechResRuleChecker<>(null)));
            put(Action.UPADTEUNIT, new UpdateUnitOwnerRuleChcker<>(new MaxUnitLevelRuleChecker<>(new SufficientUnitsRuleChecker<>(new SufficientTechResRuleChecker<>(null)) )));

        }
    };
    protected ArrayList<Action<T>> actionRecord = new ArrayList<>();
    protected ArrayList<Action<T>> levelUpRecord = new ArrayList<>();

    protected RuleChecker<T> getRuleCheckerByType(int type) {
        return rulecheckers.get(type);
    }

    // this is a function only called by server 
    public abstract Board<T> execute(Board<T> board);

    public abstract Board<T> addBaseUnitForAllPlayer(Board<T> board);
    /**
     * Do all the jobs expect for the attack action in server side
     * @param action
     * @param board
     * @param serverSide
     * @return
     */
    private String tryRecordAction(Action<T> action, Board<T> board, boolean serverSide){
        if(serverSide){
            //update the territory to the server side 
            if (action.getFrom() != null)
                action.setFrom(board.getTerritoryByName(action.getFrom().getName()));
            if (action.getTo() != null)
                action.setTo(board.getTerritoryByName(action.getTo().getName()));
            action.setPlayer(board.getPlayerById(action.getPlayerId()));
            // do check rules here
            // check if this player has done level up in this round
            if(action.getType() == Action.UPDATELEVEL){
               for(var leveUpAction :levelUpRecord){
                   if(leveUpAction.getPlayerId() == action.getPlayerId()){
                       return "You have Leveled up. Each round only allows one level up";
                   }
               }
            }
            String errorMsg = getRuleCheckerByType(action.getType()).check(action, board);
            if (errorMsg != null){
                return errorMsg;
            }
            // if the commond is commit/levelup, in order to set player to commit state in the server side
            // we need update the player which is in the server side
           // action.setPlayer(board.getPlayerById(action.getPlayerId()));
           // action.setUnits(board.getTerritoryByName(name);
            // server side need to store the action in queue
            if (action.getType() == Action.ATTACK){
                actionRecord.add(action);
            }
            else if(action.getType() == Action.UPDATELEVEL){
                levelUpRecord.add(action);
            }
        }
        // before call visitor to visit the action, set the food cost and tech cost fot this action
        action.setFoodResCost(foodCal.computeFoodResCost(action, board));
        action.setTechResCost(tecCal.computeTechResCost(action, board));
        action.accept(new ActionRecordHandler<>());
        return null;
    }

    public String tryRecordActionServer(Action<T> action, Board<T> board){
        return tryRecordAction(action, board, true);
    }

    public String tryRecordActionClient(Action<T> action, Board<T> board){
        if (action.getFrom() != null)
            action.setFrom(board.getTerritoryByName(action.getFrom().getName()));
        if (action.getTo() != null)
            action.setTo(board.getTerritoryByName(action.getTo().getName()));
        // do check rules here
        action.setPlayer(board.getPlayerById(action.getPlayerId()));
        return tryRecordAction(action, board, false);
    }

    public void reset() {
        actionRecord.clear();
        levelUpRecord.clear();
    }

}
