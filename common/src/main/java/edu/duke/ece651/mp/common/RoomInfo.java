package edu.duke.ece651.mp.common;

import java.io.Serializable;

public class RoomInfo implements Serializable {
    int gameId;
    String roomOwnerName;
    int playersNeeded;
    int playersJoined;
    boolean areYouInThisRoom;
    boolean isGameStarted;
    boolean areYouActiveInThisRoom;


    public RoomInfo(int gameId, String roomOwnerName, int playersNeeded, int playersJoined, boolean areYouInThisGame, boolean isGameStarted, boolean areYouActiveInThisRoom) {
        this.gameId = gameId;
        this.roomOwnerName = roomOwnerName;
        this.playersNeeded = playersNeeded;
        this.playersJoined = playersJoined;
        this.areYouInThisRoom = areYouInThisGame;
        this.isGameStarted = isGameStarted;
        this.areYouActiveInThisRoom = areYouActiveInThisRoom;

    }

    public int getGameId() {
        return gameId;
    }

    public String getRoomOwnerName() {
        return roomOwnerName;
    }

    public int getPlayersNeeded() {
        return playersNeeded;
    }

    public int getPlayersJoined() {
        return playersJoined;
    }

    public boolean areYouInThisRoom() {
        return areYouInThisRoom;
    }

    public boolean isGameStarted() {
        return isGameStarted;
    }

    public boolean areYouActiveInThisRoom() { return areYouActiveInThisRoom; }
}
