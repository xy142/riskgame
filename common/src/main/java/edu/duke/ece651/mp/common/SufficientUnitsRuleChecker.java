package edu.duke.ece651.mp.common;

public class SufficientUnitsRuleChecker<T> extends RuleChecker<T>{
    public SufficientUnitsRuleChecker(RuleChecker<T> next) {
        super(next);
    }

    private int getEnoughUnitsMoveAway(Action<T> action){
        for(int i =0; i< action.getUnits().getHighestLevel(); i++){
            if(action.getUnitsByLevel(i) > action.getFrom().getUnitsByLevel(i)){
                return i;
            }
        }
        return -1;
    }
    @Override
    protected String checkMyRule(Action<T> action, Board<T> board) {
        if (action.getType() == Action.PLACE) {
            if (action.getUnitsByLevel(0) <= action.getPlayer().getUnitsToPlace()) {
                return null;
            } else {
                return "You only have " + action.getPlayer().getUnitsToPlace() + " units to place.";
            }
        } else if (action.getType() == Action.ATTACK || action.getType() == Action.MOVE) {
            int shortLevel = getEnoughUnitsMoveAway(action);
            if (shortLevel==-1) {
                return null;
            } else {
                return "You only have " + action.getFrom().getUnits().getUnitsByLevel(shortLevel)  + " units in " + action.getFrom().getName() + ".";
            }
        }
        return null;
    }
}
