package edu.duke.ece651.mp.common;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * This is a abstract class to generate the board model: precoded: how many territories 
 * int the board, how they are connected, and grouped them by the player number for game
 * set up
 * The board factory will use this board model to create board
 */
public abstract class BoardModel<T> {
    // the territories in the game
    private HashMap<String, Territory<T>> terris;
    // the topology
    private HashMap<String, ArrayList<String>> neighs;
    // the grouped terris by the playerID
    private HashMap<Integer, ArrayList<String> > playerterris;

    public BoardModel(HashMap<String, Territory<T>> terris, 
    HashMap<Integer, ArrayList<String> > playerterris,
    HashMap<String, ArrayList<String>> neighs) {
        this.terris = terris;
        this.playerterris = playerterris;
        this.neighs = neighs;
    }

    public HashMap<String, Territory<T>> getterris() {
        return terris;
    }
    public HashMap<String, ArrayList<String>> getneighs() {
        return neighs;
    }

    public HashMap<Integer, ArrayList<String>> getPlayerterris() {
        return playerterris;
    }


    
}
