package edu.duke.ece651.mp.common;
//Visitor for Action class
public interface ActionHandler<T> {
     public void visit(AttackAction<T> updateLevelAction);
     public void visit(MoveAction<T> move);
     public void visit(PlaceAction<T> place);
     public void visit(CommitAction<T> commit);
     public void visit(UpdateLevelAction<T> updateLevel);
     public void visit(UpdateUnitAction<T> updateUnitAction);
}
