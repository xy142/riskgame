package edu.duke.ece651.mp.common.packet;

import edu.duke.ece651.mp.common.*;

import java.io.IOException;

public class CheckResultPacket extends Packet {
    private String errorMsg;
    private Action<?> action;
    public CheckResultPacket(String username, int playerId, String errorMsg, Action action) {
        super(username, playerId);
        this.errorMsg = errorMsg;
        this.action = action;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public <T> Action<T> getAction() {
        return (Action<T>) action;
    }
    @Override
    public void accept(PacketHandler ph) throws IOException, InterruptedException {
        ph.visit(this);
    }
}
