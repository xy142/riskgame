package edu.duke.ece651.mp.common;

public class ClientExecutor<T> extends Executor<T> {

    @Override
    public Board<T> execute(Board<T> board) {
        throw new RuntimeException("Client does not need to execute actions");
    }
   /**
   * This Method get Execeute the Recording of the Action by
   * determining the whether or not the action can execute. This is determined by 
   * checking whether the correct number of units are available for To and From Territories
   * @param 
   * @throws  
   */
    // @Override
    // public String tryRecordAction(Action<T> action, Board<T> board) {
    //     action.accept(new ActionRecordHandler<>());
    //     return null;
    // }

    @Override
    public Board<T> addBaseUnitForAllPlayer(Board<T> board) {
        throw new RuntimeException("Client does not need to execute actions");
    }
}
