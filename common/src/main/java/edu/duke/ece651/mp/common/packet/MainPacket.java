package edu.duke.ece651.mp.common.packet;

import java.io.IOException;

public class MainPacket extends InstructionPacket {
    public MainPacket(String username, int playerId) {
        super(username, playerId);
    }

    @Override
    public void accept(PacketHandler ph) throws IOException {
        ph.visit(this);
    }
}
