package edu.duke.ece651.mp.common;

public interface BoardViewInterface<T> {
  /*This method shows the border of the Territory
   @returns the result in a String
  */
  public String showBorder();
  /*This method shows any Territory 
   @returns the result in a String
*/
  public String showTerritory();
  public String OneDisplay(int player_id);
  public String getDisplay();
}
