package edu.duke.ece651.mp.common;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class BoardView<T> implements BoardViewInterface<T> {

    private Board<T> board;

    public BoardView(Board<T> board) {
        this.board = board;
    }
  /*This Method displays the All players Territory info
    @param 
    @return All Players territory information
   */
  @Override
   public String getDisplay() {
        Map<Integer, ArrayList<Territory<T>>> playersToTerritories = new HashMap<>();
        for (var territory : board.getAllTerritory()) {
          if (!playersToTerritories.containsKey(territory.getOwnerId())){
                playersToTerritories.put(territory.getOwnerId(), new ArrayList<>());
                playersToTerritories.get(territory.getOwnerId()).add(territory);
          }else{
             playersToTerritories.get(territory.getOwnerId()).add(territory);
          }
        }
        String display = GatherTerritoies(playersToTerritories);
        return display;
    }

  /*This Method displays the Territory  info of the Player that calls it
    @param Player_id
    @return Players territory information
   */
  
  @Override
  public String OneDisplay(int player_id){
       Map<Integer, ArrayList<Territory<T>>> playersToTerritories = new HashMap<>();
        for (var territory : board.getAllTerritory()) {
          if (!playersToTerritories.containsKey(territory.getOwnerId()) && player_id == territory.getOwnerId()){
                playersToTerritories.put(territory.getOwnerId(), new ArrayList<>());
                playersToTerritories.get(territory.getOwnerId()).add(territory);
          }else if (player_id == territory.getOwnerId()){
             playersToTerritories.get(territory.getOwnerId()).add(territory);
          }

        }
        String display = GatherTerritoies(playersToTerritories);
        return display;

  }
  /**
   * displays all the players and their territories repectively
   * @param playersToTerritories
   * @return
   */
  private String GatherTerritoies(Map<Integer, ArrayList<Territory<T>>> playersToTerritories){
       String display = "";
        for (var pair : playersToTerritories.entrySet()) {
            display += getOnePlayerDisplay(pair);
            display += '\n';
            }
        return display;
  }

  /**
   * display the territorries  a player has
   * @param pair the entry of this player and its territories
   * @return
   */
    private String getOnePlayerDisplay(Map.Entry<Integer, ArrayList<Territory<T>>> pair) {
        String display = "";
        display += getPlayerHeader(board.getPlayerById(pair.getKey()));
        display += "--------------------------------\n";
        display += getTerritoriesDisplay(pair.getValue());
        display += "\n";
        return display;
    }
    /**
     * Display the territories info of one player: units, neighbours
     */
    private String getTerritoriesDisplay(ArrayList<Territory<T>> territories) {
        String display = "";
        for (var territory: territories) {
            //v1: "n units in Oz (next to A, B ,C)"
            // v2: "[1,2,3,4,5,6,7] units in Oz (next to A, B, C)"
            display += getTerriUnitsDisplay(territory) + " units in " + territory.getName() + " (next to:" + getNeighborsDisplay(territory) +")\n";
        }
        return display;
    }
    private String getTerriUnitsDisplay(Territory<T> terri){
      String display = "[";

      for (int i =0; i < terri.getUnits().getHighestLevel(); i++){
        display += terri.getUnitsByLevel(i);
        if(i != terri.getUnits().getHighestLevel()-1){
          display +=  ",";
        }
      }
      display +="]";
      return display;

    }

    /**
     * called by getTerritoriesDisplay() to display neighbours' name
     * @param territory
     * @return
     */
    private String getNeighborsDisplay(Territory<T> territory) {
        String display = "";
        for (var neigh : board.getTerritoryNeighbour(territory.getName())) {
            display += " ";
            display += neigh.getName();
            display += ",";
        }
        return display;
    }

    /**
     * display player's name
     * @param player
     * @return
     */
    private String getPlayerHeader(Player player) {
        return player.getPlayerColor() + " " + "player:\n";
    }

    @Override
    public String showBorder() {
      String message = "This is your Border:\n";
      return message;
    }

    @Override
    public String showTerritory() {
      String message = "This is your Territory:\n";
      return message;
    }
}


