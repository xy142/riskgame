package edu.duke.ece651.mp.common.packet;

import java.io.IOException;

public class EnterRoomPacket extends Packet {
    private int gameId;
    public EnterRoomPacket(String username, int playerId, int gameId) {
        super(username, playerId);
        this.gameId = gameId;
    }

    public int getGameId() {
        return gameId;
    }

    @Override
    public void accept(PacketHandler ph) throws IOException, InterruptedException {
        ph.visit(this);
    }
}
