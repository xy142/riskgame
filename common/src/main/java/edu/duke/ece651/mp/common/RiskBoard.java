package edu.duke.ece651.mp.common;

import java.util.*;

public class RiskBoard<T> implements Board<T> {
    /**
     * A hashmap stores all the terris
     * The key is the name for this terri
     * The value is a pointer that point at the terri
     */
    private final HashMap<String, Territory<T>> territories;

    /**
     * A hashmap stores all the neighbour terris for the terri
     * The key is the Terri
     * The values are its neighbour terris
     */
    private final HashMap<String, ArrayList<String>> neighTerris;

    /**
     * A hashmap stores all the player's terris
     * It is need to be initilized at the beginning by another function
     * The key is the playerId
     * The value is the list of Terris that player owns
     */
    private HashMap<Integer, ArrayList<String>> playerTerris;

    /**
     * The arrayList stores all the player
     */
    private List<Player> playerList;

    public RiskBoard(HashMap<String, Territory<T>> territories,
            HashMap<String, ArrayList<String>> neighTerris,
            HashMap<Integer, ArrayList<String>> playerTerris,
            List<Player> playerList2) {
        this.territories = new HashMap<>();
        for (String terri : territories.keySet()) {
            this.territories.put(terri, (Territory<T>) new V1Territory(terri));
        }
        this.neighTerris = neighTerris;
        this.playerTerris = playerTerris;
        this.playerList = playerList2;
        // playerToTerritories = new HashMap<>();

    }

    /**
     * This is constructor is only for the goal 1.2 test
     */
    public RiskBoard(HashMap<String, Territory<T>> territories) {
        this(territories, new HashMap<String, ArrayList<String>>(),
                new HashMap<Integer, ArrayList<String>>(),
                new ArrayList<Player>());
    }

    @Override
    public void initTerrisOwnerByGroup() {
        HashMap<Integer, ArrayList<String>> playerTerris2 = new HashMap<>();
        for (int i = 0; i < getPlayerNum(); ++i) {
            for (String terri : playerTerris.get(i)) {
                territories.get(terri).setOwnerId(playerList.get(i).getId());
            }
            playerTerris2.put(playerList.get(i).getId(), playerTerris.get(i));
        }
        playerTerris = playerTerris2;
//        for (int i = 0; i < getPlayerNum(); i++) {
//            for (String terri : playerTerris.get(i)) {
//                territories.get(terri).setOwnerId(i);
//            }
//        }

    }

    private boolean dfs(HashSet<String> visited, String curr, String dest, int ownerId) {
        if (curr.equals(dest)) {
            return true;
        }
        boolean ans = false;
        // check its adjacent territories
        for (String terri : neighTerris.get(curr)) {
            if (visited.contains(terri) || getTerritoryByName(terri).getOwnerId() != ownerId) {
                continue;
            }
            visited.add(terri);
            ans = dfs(visited, terri, dest, ownerId);
            if (ans) {
                break;
            }
        }
        return ans;
    }

    // using dfs to check if two terris are connected
    @Override
    public boolean isConnected(String terri1, String terri2) {
        HashSet<String> visited = new HashSet<>();
        visited.add(terri1);
        return dfs(visited, terri1, terri2, getTerritoryByName(terri1).getOwnerId());
    }

    @Override
    public boolean isAdjacent(String terri1, String terri2) {
        for (String it : neighTerris.get(terri1)) {
            if (it.equals(terri2)) {
                return true;
            }
        }
        return false;
    }

    /**
     * return null if not find
     */
    @Override
    public Territory<T> getTerritoryByName(String name) {
        return territories.get(name);
    }

    @Override
    public Collection<Territory<T>> getTerritoryNeighbour(String territory) {
        Collection<Territory<T>> ans = new ArrayList<Territory<T>>();
        for (String i : neighTerris.get(territory)) {
            ans.add(getTerritoryByName(i));
        }
        return ans;
    }

    @Override
    public Collection<Territory<T>> getAllTerritory() {
        return territories.values();
    }

    @Override
    public Collection<Territory<T>> getTerritoryByPlayerId(int id) {
        Collection<Territory<T>> ans = new ArrayList<Territory<T>>();
        for (String i : playerTerris.get(id)) {
            ans.add(getTerritoryByName(i));
        }
        return ans;
    }

    public int getPlayerNum() {
        return playerList.size();
    }

    @Override
    public boolean ifTerritoryExit(String name) {
        if (getTerritoryByName(name) == null) {
            return false;
        }
        return true;
    }

    @Override
    public Player getPlayerById(int id) {
        for (var player : playerList) {
            if (player.getId() == id) {
                return player;
            }
        }
        System.out.println("getPlayerById() is wrong: id is invalid");
        return null;
    }

    /**
     * The terri's owner filed has been updated, but the playerTerris not yet
     */
    @Override
    public void updatePlayerTerris(String terri, int oldOwner) {
        int newOwner = getTerritoryByName(terri).getOwnerId();
        // 1. delete it from the old arraylist of oldOwner
        playerTerris.get(oldOwner).remove(terri);
        // 2. add it to the new arraylist of newOwner
        playerTerris.get(newOwner).add(terri);
    }

    @Override
    public void initSizeForTerris() {
        for (var player : playerList) {
            int terriNum = getTerritoryByPlayerId(player.getId()).size();
            int avgSize = 10 / terriNum;
            int lastSize = (10 % terriNum) + avgSize;
            for (int i = 0; i < terriNum; i++) {
                int size =avgSize;
                if (i == terriNum - 1) { // last size
                    size = lastSize;
                }   
                getTerritoryByName(playerTerris.get(player.getId()).get(i)).setSize(size);
            }
        }

    }

    @Override
    public void initFoodProduction() {
        for(var terri :territories.values()){
            terri.setFoodProduction(terri.getSize());
        }
        
    }

    @Override
    public void initTechProduction() {
        for(var terri :territories.values()){
            //TODO: tech production is hardcoded here 
            terri.setTechProduction(terri.getSize()* 5);
        }
    }

}
