package edu.duke.ece651.mp.common.packet;

import java.io.IOException;

public class CreateRoomPacket extends Packet {
    private int roomSize;
    public CreateRoomPacket(String username, int playerId, int roomSize) {
        super(username, playerId);
        this.roomSize = roomSize;
    }

    public int getRoomSize() {
        return roomSize;
    }

    @Override
    public void accept(PacketHandler ph) throws IOException, InterruptedException {
        ph.visit(this);
    }
}
