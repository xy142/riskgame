package edu.duke.ece651.mp.common.packet;

import java.io.IOException;

public class LogOutPacket extends Packet {
    public LogOutPacket(String username, int playerId) {
        super(username, playerId);
    }

    @Override
    public void accept(PacketHandler ph) throws IOException, InterruptedException {
        ph.visit(this);
    }
}
