package edu.duke.ece651.mp.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * This is the interface for Territory
 */
public interface Territory<T> extends Serializable {

    /**
     * @return the name of the terri
     */
    public String getName();

    /**
     * @return the player-id for the terri
     */
    public int getOwnerId();

    /**
     * @return the units
     */
    public Unit getUnits();

    public void setUnits(Unit another);

    public int getUnitsByLevel(int level);

    public int getSize();
    public void setSize( int size);

    public int getFoodProduction();

    public void setFoodProduction(int foodProduction);

    public int getTechProduction();

    public void setTechProduction(int techProduction);

    /**
     * Set the player for this terri
     * @param playerId
     */
    public void setOwnerId(int playerId);
   // public List<Territory<T>> getNeighbors();
    //public Player getPlayer();
    //public void setPlayer(Player player);

    /**
     * 
     * @param unitsToSet set the units for the terri
     */
    public void setUnitsByLevel(int level, int unitsToSet);

   // public void setNeighbours(ArrayList<Territory<T>> neighbours);

    /**
     * 
     * @param o the compared object 
     * @return true if the names of the Terri are equal
     */
    public boolean equals(Object o);
}
