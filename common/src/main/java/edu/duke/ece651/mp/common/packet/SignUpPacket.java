package edu.duke.ece651.mp.common.packet;

import java.io.IOException;

public class SignUpPacket extends Packet {
    private String username;
    private String password;
    public SignUpPacket(String empty, int playerId, String username, String password) {
        super(empty, playerId);
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public void accept(PacketHandler ph) throws IOException, InterruptedException {
        ph.visit(this);
    }
}
