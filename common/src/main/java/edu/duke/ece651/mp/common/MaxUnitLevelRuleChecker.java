package edu.duke.ece651.mp.common;

public class MaxUnitLevelRuleChecker<T> extends RuleChecker<T> {

    public MaxUnitLevelRuleChecker(RuleChecker<T> next) {
        super(next);
    }

    @Override
    protected String checkMyRule(Action<T> action, Board<T> board) {
        // the player initial level is 1, max level is 6
        // the units initial level is 1, max level is 6
        // level 1 player can update units 0->1
        //level 6 player can update units 5->6
        // can't update level 6 units, which is already the highest
        int playerLevel = action.getPlayer().getTechLevel();
        for ( int i = playerLevel; i < action.getUnits().getHighestLevel(); i++){
           if( action.getUnitsByLevel(i) !=0){
               return "You can not update units in this level";
           }
        }
        return null;
    }
    
}
